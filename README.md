# YouTube Downloader

This is an application through which you can download music and videos from YouTube.\
The application uses the youtube-dl library and converts the video through ffmpeg. All dependencies are part of the application.\
Thanks to this, the application is **only for Windows systems!**

```mermaid
graph LR
A[YouTube Downloader] --> B{Task}
B --> C(YouTube)
D((Your Video URL)) --> A
B --> E(youtube-dl)
B --> F(ffmpeg)
F --> G(ffprobe)
```

[![Donate with PayPal](donate-pp.png)](https://www.paypal.com/donate?hosted_button_id=NZFDH9C3RX5A6)

# Download

Go to [**Tags**](https://gitlab.com/OphiteCorp/youtube-downloader/tags) and download the latest version\
![](media/gitlab-download.png)

# Requirements

- Java 13 (JRE or JDK) and can be used by oracle, openjdk or ibm. [**You can download Java from oracle here**](https
://www.oracle.com/technetwork/java/javase/downloads/index.html)
- Windows 7 or later

# How to use it

Double-tap to open the *.jar application file.

The application can be run with parameters:
- `-clean` = At startup it clears all the temp it uses. Each running instance uses the same temp directory to restrict the write to disk.
- `-threads=N` = Allows you to set the maximum number of parallel threads that process videos. Replace the value 'N' with the number of threads. The default value is 5, which is sufficient.

# Screenshot

![](media/progress.png)

# Video

![](media/youtube-downloader.mp4)

# Third party applications

- [youtube-dl](https://github.com/rg3/youtube-dl)
- [ffmpeg + ffprobe](https://www.ffmpeg.org/)

> These applications are part of this application. So it is possible to replace them with a newer version and the application should still work the same way.

# Copyright

The application is created under MIT license, so everyone can participate in the development of the application and their modifications. It just can not be sold for money or other currency.

I wish a lot of downloaded videos **;-)**