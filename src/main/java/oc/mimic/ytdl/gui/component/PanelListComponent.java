package oc.mimic.ytdl.gui.component;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel obsahující seznam dalších komponent.
 *
 * @author mimic
 */
public final class PanelListComponent extends JPanel {

  private final List<Component> items = new ArrayList<>();
  private final JPanel itemsPane;

  public PanelListComponent() {
    super();

    setLayout(new BorderLayout());

    var gbc = new GridBagConstraints();
    gbc.gridwidth = GridBagConstraints.REMAINDER;
    gbc.weightx = 1;
    gbc.weighty = 1;

    itemsPane = new JPanel(new GridBagLayout());
    itemsPane.add(new JPanel(), gbc);

    var scroll = new JScrollPane(itemsPane);
    scroll.getVerticalScrollBar().setUnitIncrement(16);
    scroll.setBorder(BorderFactory.createEmptyBorder());

    add(scroll);
  }

  /**
   * Přidá novou položku do seznamu.
   *
   * @param item
   */
  public void addItem(Component item) {
    synchronized (items) {
      var gbc = new GridBagConstraints();
      gbc.gridwidth = GridBagConstraints.REMAINDER;
      gbc.weightx = 1;
      gbc.fill = GridBagConstraints.HORIZONTAL;

      itemsPane.add(item, gbc, 0);

      if (!(item instanceof Box.Filler)) {
        items.add(item);
      }
      validate();
      repaint();
    }
  }

  /**
   * Odebere položku ze seznamu podle jména
   *
   * @param compName
   */
  public void removeItemByName(String compName) {
    synchronized (items) {
      var comps = itemsPane.getComponents();

      for (var i = comps.length - 1; i >= 0; --i) {
        var c = comps[i];

        if (c.getName() != null && c.getName().equals(compName)) {
          items.remove(c);
          itemsPane.remove(i);

          if (comps[i - 1] instanceof Box.Filler) {
            itemsPane.remove(i - 1);
          }
          break;
        }
      }
      validate();
      repaint();
    }
  }

  /**
   * Získá počet položek v seznamu.
   *
   * @return
   */
  public int getItemsCount() {
    return items.size();
  }
}
