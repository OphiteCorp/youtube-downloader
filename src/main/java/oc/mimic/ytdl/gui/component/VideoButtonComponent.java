package oc.mimic.ytdl.gui.component;

import oc.mimic.ytdl.ResourcesProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Tlačítko u videa v seznamu videí.
 *
 * @author mimic
 */
public final class VideoButtonComponent extends JButton {

  private static final long serialVersionUID = 7513491454618354964L;

  private static final Font FONT;

  static {
    FONT = ResourcesProvider.getInstance().getFont(ResourcesProvider.RES_CODE_FONT);
  }

  private Color color = Color.BLACK;
  private Color hoverColor = Color.BLACK;

  /**
   * @param text
   * @param color
   */
  public VideoButtonComponent(String text, Color color) {
    super(text);

    setFont(FONT.deriveFont(Font.BOLD, 10));
    setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 5));
    setContentAreaFilled(false);
    setForeground(new Color(0, 0, 0));
    setForeground(color);

    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseEntered(MouseEvent e) {
        setForeground(hoverColor);
      }

      @Override
      public void mouseExited(MouseEvent e) {
        setForeground(color);
      }
    });
  }

  public Color getColor() {
    return color;
  }

  public void setColor(Color color) {
    this.color = color;
  }

  public Color getHoverColor() {
    return hoverColor;
  }

  public void setHoverColor(Color hoverColor) {
    this.hoverColor = hoverColor;
  }
}
