package oc.mimic.ytdl.gui.dto;

/**
 * Položka v comboBoxu.
 *
 * @author mimic
 */
public class ComboBoxItem {

  private final Object key;
  private String label;

  /**
   * @param key
   * @param label
   */
  public ComboBoxItem(Object key, String label) {
    this.key = key;
    this.label = label;
  }

  public Object getKey() {
    return key;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return label;
  }
}
