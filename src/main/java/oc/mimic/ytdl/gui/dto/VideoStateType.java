package oc.mimic.ytdl.gui.dto;

/**
 * Typ stavu videa. Slouží pouze pro GUI.
 *
 * @author mimic
 */
public enum VideoStateType {

  READY("Ready!"),
  VALIDATING("Checking input data..."),
  FETCHING("Fetching '%s' video informations..."),
  OPENING_FORMAT_SELECTION("Opening format selection..."),
  WAITING_FOR_FORMAT_SELECTION("Waiting for the format selection..."),
  COLLECTING_FOR_DOWNLOAD_MP3("Collecting information for download mp3..."),
  COLLECTING_FOR_DOWNLOAD_BEST_VIDEO("Collecting information for download best video..."),
  CREATING_DOWNLOAD("Creating a video download process...");

  private final String format;

  /**
   * @param format Formát pro String.format()
   */
  VideoStateType(String format) {
    this.format = format;
  }

  public String getFormat() {
    return format;
  }
}
