package oc.mimic.ytdl.gui.dto;

import oc.mimic.ytdl.logic.json.VideoFormatJson;
import oc.mimic.ytdl.logic.types.CustomAudioType;
import oc.mimic.ytdl.logic.types.CustomVideoType;
import oc.mimic.ytdl.logic.types.FormatType;

import java.awt.*;

/**
 * Informace o vybraném formátu.
 *
 * @author mimic
 */
public final class FormatSelectionData {

  private final VideoFormatJson format;
  private final FormatType formatType;
  private CustomAudioType customAudioType;
  private CustomVideoType customVideoType;
  private String shortName;
  private String outputFile;
  private Image thumbnail;

  public FormatSelectionData(FormatType formatType, VideoFormatJson format) {
    this.formatType = formatType;
    this.format = format;
  }

  public VideoFormatJson getFormat() {
    return format;
  }

  public FormatType getFormatType() {
    return formatType;
  }

  public CustomAudioType getCustomAudioType() {
    return customAudioType;
  }

  public void setCustomAudioType(CustomAudioType customAudioType) {
    this.customAudioType = customAudioType;
  }

  public String getOutputFile() {
    return outputFile;
  }

  public void setOutputFile(String outputFile) {
    this.outputFile = outputFile;
  }

  public CustomVideoType getCustomVideoType() {
    return customVideoType;
  }

  public void setCustomVideoType(CustomVideoType customVideoType) {
    this.customVideoType = customVideoType;
  }

  public Image getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(Image thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }
}
