package oc.mimic.ytdl.gui.dto;

import oc.mimic.ytdl.logic.json.VideoFormatJson;
import oc.mimic.ytdl.logic.types.FormatType;
import oc.mimic.ytdl.logic.types.OutputFileSuffixType;

/**
 * Položka v comboBoxu reprezentující formát videa.
 *
 * @author mimic
 */
public final class ComboBoxFormatItem extends ComboBoxItem {

  private final VideoFormatJson format;
  private final FormatType formatType;
  private OutputFileSuffixType suffix;
  private String shortName;

  /**
   * @param formatType
   * @param format
   * @param key
   * @param label
   */
  public ComboBoxFormatItem(FormatType formatType, VideoFormatJson format, Object key, String label) {
    super(key, label);
    this.formatType = formatType;
    this.format = format;
  }

  public VideoFormatJson getFormat() {
    return format;
  }

  public FormatType getFormatType() {
    return formatType;
  }

  public OutputFileSuffixType getSuffix() {
    return suffix;
  }

  public void setSuffix(OutputFileSuffixType suffix) {
    this.suffix = suffix;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }
}
