package oc.mimic.ytdl.gui.dto;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Informace o dávce.
 *
 * @author mimic
 */
public final class BatchData {

  public static final String TYPE_MP3 = "mp3";
  public static final String TYPE_VIDEO = "video";
  public static final List<String> SUPPORTED_TYPES = Arrays.asList(TYPE_MP3, TYPE_VIDEO);

  private String type;
  private String url;

  /**
   * @param type
   * @param url
   */
  public BatchData(String type, String url) {
    this.type = type;
    this.url = url;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    var batchData = (BatchData) o;
    return Objects.equals(type, batchData.type) && Objects.equals(url, batchData.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, url);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", BatchData.class.getSimpleName() + "[", "]").add("type='" + type + "'")
            .add("url='" + url + "'").toString();
  }
}
