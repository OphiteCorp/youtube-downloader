package oc.mimic.ytdl.gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import oc.mimic.ytdl.ResourcesProvider;
import oc.mimic.ytdl.config.ApplicationConfig;
import oc.mimic.ytdl.config.HistoryConfig;
import oc.mimic.ytdl.config.HistoryVideoConfig;
import oc.mimic.ytdl.gui.component.PanelListComponent;
import oc.mimic.ytdl.gui.component.VideoButtonComponent;
import oc.mimic.ytdl.gui.dto.BatchData;
import oc.mimic.ytdl.gui.dto.ComboBoxItem;
import oc.mimic.ytdl.gui.dto.FormatSelectionData;
import oc.mimic.ytdl.gui.dto.VideoStateType;
import oc.mimic.ytdl.logic.AudioPlayer;
import oc.mimic.ytdl.logic.DumpVideoData;
import oc.mimic.ytdl.logic.Utils;
import oc.mimic.ytdl.logic.json.VideoInfoJson;
import oc.mimic.ytdl.logic.types.EncodingType;
import oc.mimic.ytdl.task.DownloadTask;
import oc.mimic.ytdl.task.DownloadTaskConfig;
import oc.mimic.ytdl.task.IDownloadTaskListener;
import oc.mimic.ytdl.task.core.ITaskListener;
import oc.mimic.ytdl.task.core.Task;
import oc.mimic.ytdl.task.core.TaskManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Hlavní okno aplikace.
 *
 * @author mimic
 */
public final class MainForm extends JFrame {

    private static final long serialVersionUID = 7733617244708820321L;
    private static final Logger LOG = LoggerFactory.getLogger(MainForm.class);
    private static final String YOUTUBE_URL = "https://www.youtube.com/watch?v=";
    private static final Object LOCK = new Object();

    public static final Cursor HAND_CURSOR = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

    private static final Pattern PATTERN_VIDEO_URL;
    private static final Font CODE_FONT;
    private static final Font UNI_FONT;

    static {
        PATTERN_VIDEO_URL = Pattern.compile("(.+youtube\\.com/watch\\?v=[^&]+)");
        CODE_FONT = ResourcesProvider.getInstance().getFont(ResourcesProvider.RES_CODE_FONT);
        UNI_FONT = ResourcesProvider.getInstance().getFont(ResourcesProvider.RES_UNI_FONT);
    }

    private JPanel rootPane;
    private JPanel topPane;
    private JPanel globalPane;
    private JPanel progressPane;
    private JTextField tfVideo;
    private JTextField tfOutputDir;
    private JComboBox<ComboBoxItem> boxEncoding;
    private JCheckBox cbUseHttp;
    private JCheckBox cbDownloadThumb;
    private JCheckBox cbAddMetadata;
    private JButton btnFetch;
    private JButton btnSelectDir;
    private JButton btnBatch;
    private JButton btnDonate;
    private JButton btnGoDirectory;
    private JButton btnDownloadMp3;
    private JButton btnDownloadBestVideo;
    private JLabel lbState;
    private JLabel lbCopy;

    private final TaskManager tm;
    private final PanelListComponent listComponent = new PanelListComponent();
    private final ApplicationConfig config;
    private final HistoryConfig history;
    private int counter;

    public MainForm(ApplicationConfig config, HistoryConfig history) {
        super();
        this.config = config;
        this.history = history;
        tm = TaskManager.getInstance(config);
        LOG.debug("Loading the main window");

        setTitle("[OC] YouTube Downloader");
        setSize(1050, 400);
        setMinimumSize(new Dimension(1050, 400));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        cbAddMetadata.setSelected(config.isAddMetadata());
        cbDownloadThumb.setSelected(config.isDownloadThumbnail());
        cbUseHttp.setSelected(config.isUseHttp());

        var image = ResourcesProvider.getInstance().getImage(ResourcesProvider.RES_ICON);
        setIconImage(image);

        setContentPane(rootPane);
        selectDefaultOutputDirectory();
        progressPane.add(listComponent, BorderLayout.CENTER);

        // další nastavení
        btnSelectDir.setCursor(HAND_CURSOR);
        btnGoDirectory.setCursor(HAND_CURSOR);
        btnBatch.setCursor(HAND_CURSOR);
        btnFetch.setCursor(HAND_CURSOR);
        btnDownloadMp3.setCursor(HAND_CURSOR);
        btnDownloadBestVideo.setCursor(HAND_CURSOR);
        btnDonate.setCursor(HAND_CURSOR);
        cbAddMetadata.setCursor(HAND_CURSOR);
        cbDownloadThumb.setCursor(HAND_CURSOR);
        cbUseHttp.setCursor(HAND_CURSOR);

        // události;
        btnSelectDir.addActionListener(e -> selectDirectoryAction());
        btnGoDirectory.addActionListener(e -> goSelectDirectoryAction());
        btnBatch.addActionListener(e -> openBatchAction());
        btnFetch.addActionListener(e -> fetchVideoAction());
        btnDownloadMp3.addActionListener(e -> downloadMp3Action(null));
        btnDownloadBestVideo.addActionListener(e -> downloadBestVideoAction(null));
        btnDonate.addActionListener(e -> Utils.openBrowser("https://www.paypal.com/donate?hosted_button_id=NZFDH9C3RX5A6"));

        // výchozí nastavení
        fillEncoding();
        invokeNewState(VideoStateType.READY);

        // nastavení verze
        var version = ResourcesProvider.getInstance().readAppVersion();
        lbCopy.setText("by mimic | 2020 | v" + version + " ");

        // nastavení darování
        var donateImg = ResourcesProvider.getInstance().getImage(ResourcesProvider.RES_DONATE);
        btnDonate.setBorder(BorderFactory.createEmptyBorder());
        btnDonate.setContentAreaFilled(false);
        btnDonate.setIcon(new ImageIcon(donateImg));
        btnDonate.setText("");

        pack();
        setLocationRelativeTo(this);

        // přidá všechny videa z historie
        addHistoryVideos();
        updateProgressTitle();
        startUpdateProgressThread();

        LOG.debug("Loaded");
    }

    /**
     * Oteře explorer v na cílovém adresáři.
     */
    private void goSelectDirectoryAction() {
        var dir = tfOutputDir.getText();
        if (dir.isBlank()) {
            dir = ".";
        }
        try {
            Desktop.getDesktop().open(new File(dir));
        } catch (IOException e) {
            LOG.error("An unexpected error occurred while opening the directory: {}", dir);
            JOptionPane.showMessageDialog(this, "Error opening directory. More information in the log.", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Validace dat při stisku tlačítko pro fetch nebo stažení mp3.
     *
     * @param videoUrl Video URL.
     * @return
     */
    private String preValidation(String videoUrl) {
        invokeNewState(VideoStateType.VALIDATING);

        var video = (videoUrl != null) ? videoUrl : tfVideo.getText().strip();
        video = video.replaceAll(" ", "");

        // kontrola vstupního videa
        if (video.isBlank()) {
            LOG.warn("Video Input URL not filled in");
            JOptionPane.showMessageDialog(this, "Missing URL for video on youtube.", "Warning", JOptionPane.WARNING_MESSAGE);
            invokeNewState(VideoStateType.READY);
            return null;
        }
        // kontrola vstupního výstupního adresáře
        if (tfOutputDir.getText().isBlank()) {
            LOG.warn("The output directory was not defined");
            JOptionPane
                    .showMessageDialog(this, "Missing to set the output directory.", "Warning", JOptionPane.WARNING_MESSAGE);
            invokeNewState(VideoStateType.READY);
            return null;
        }
        if (!video.toLowerCase().contains("youtube.com")) {
            video = YOUTUBE_URL + video;
        }
        // kontrola správného formátu videa
        var match = PATTERN_VIDEO_URL.matcher(video);
        if (match.find() && match.groupCount() > 0) {
            video = match.group(1);
        } else {
            LOG.warn("Invalid URL. Enter URL to YouTube video");
            JOptionPane.showMessageDialog(this, "Invalid URL for video on youtube.", "Warning", JOptionPane.WARNING_MESSAGE);
            invokeNewState(VideoStateType.READY);
            return null;
        }
        return video;
    }

    /**
     * Získá informace o videu.
     *
     * @param video URL na video.
     * @return
     */
    private VideoInfoJson fetchVideoInfo(String video) {
        LOG.info("Getting video information started");
        var dump = new DumpVideoData(video);
        var videoInfo = dump.call();

        if (videoInfo == null) {
            LOG.error("Invalid video code or URL: {}", tfVideo.getText());
            JOptionPane.showMessageDialog(this, "Invalid video code or URL.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            if (videoInfo.isLive()) {
                LOG.error("Video '{}' is being broadcast live. This video can not be fetched", video);
                JOptionPane
                        .showMessageDialog(this, "The video is live, wait for the live broadcast to end or try another video.",
                                "Error", JOptionPane.ERROR_MESSAGE);
                videoInfo = null;
            }
        }
        return videoInfo;
    }

    /**
     * Připraví download tásk a vytvoří položku v seznamu ke zpracování.
     *
     * @param videoInfo
     * @param selectionData
     * @return
     */
    private DownloadTask prepareDownloadTask(VideoInfoJson videoInfo, FormatSelectionData selectionData) {
        if (config.isSoundsOn()) {
            var stream = ResourcesProvider.getInstance().load(ResourcesProvider.RES_ESTABLISHED_SOUND);
            AudioPlayer.play(stream);
        }
        var encoding = ((ComboBoxItem) boxEncoding.getSelectedItem()).getKey();
        var config = new DownloadTaskConfig();
        config.setUrlOrVideoHash(videoInfo.getWebPageUrl());
        config.setEncoding(encoding != null ? encoding.toString() : null);
        config.setUseHttp(cbUseHttp.isSelected());
        config.setDownloadThumbnail(cbDownloadThumb.isSelected());
        config.setAddMetadata(cbAddMetadata.isSelected());
        config.setOutputFile(selectionData.getOutputFile());
        config.setFormat(selectionData.getFormat());
        config.setFormatType(selectionData.getFormatType());
        config.setCustomAudioType(selectionData.getCustomAudioType());
        config.setCustomVideoType(selectionData.getCustomVideoType());

        var task = new DownloadTask(config);
        var itemData = createVideoItem(task, selectionData, videoInfo);
        var handler = new DownloadHandler(itemData);

        task.addListener(handler);
        task.addDownloadListener(handler);

        listComponent.addItem(itemData.itemPane);
        listComponent.addItem(Box.createVerticalStrut(6));
        updateProgressTitle();

        // přidání do historie
        if (this.config.isHistory()) {
            var video = new HistoryVideoConfig();
            video.setTitle(videoInfo.getTitle());
            video.setVideoUrl(videoInfo.getWebPageUrl());
            video.setOutput(selectionData.getOutputFile());
            video.setDuration(videoInfo.getDuration());
            video.setShortName(selectionData.getShortName());
            video.setFormatType(selectionData.getFormatType().getLabel());
            video.setDone(false);

            var image = Utils.readImage(videoInfo.getThumbnailUrl());
            image = Utils.scaleImage(image, 64, 64);
            var imageBase64 = Utils.convertToBase64(Utils.toBufferedImage(image));
            video.setImage(imageBase64);

            synchronized (this) {
                history.addVideo(video);
            }
        }
        return task;
    }

    /**
     * Přidá videa z historie.
     */
    private void addHistoryVideos() {
        if (config.isHistory() && !history.getVideos().isEmpty()) {
            for (var video : history.getVideos()) {
                var itemData = createVideoItem(video);
                listComponent.addItem(itemData.itemPane);
                listComponent.addItem(Box.createVerticalStrut(6));
            }
            updateProgressTitle();
        }
    }

    /**
     * Získá informace o videu a umožní výběr formátu ke stažení.
     */
    private void fetchVideoAction() {
        final var videoCode = preValidation(tfVideo.getText().strip());
        if (videoCode == null) {
            return;
        }
        invokeNewState(VideoStateType.FETCHING, new Object[]{videoCode}, false);

        new Thread(() -> {
            var videoInfo = fetchVideoInfo(videoCode);
            if (videoInfo != null) {
                LOG.info("Information was successfully obtained. Opening a selection window to download");
                invokeNewState(VideoStateType.OPENING_FORMAT_SELECTION);

                var dialog = new FormatSelectionDialog(this, config, videoInfo, tfOutputDir.getText());
                dialog.setVisible(true);
                var selectionData = dialog.getFormatSelectionData();

                if (selectionData != null) {
                    invokeNewState(VideoStateType.CREATING_DOWNLOAD);
                    LOG.info("Video format selected. Downloading will start");

                    var task = prepareDownloadTask(videoInfo, selectionData);
                    tm.execute(task);
                }
            }
            invokeNewState(VideoStateType.READY);
        }).start();
    }

    /**
     * Akce volaná při požadavku na stažení mp3 bez výběru formátu.
     *
     * @param videoUrl URL na video (může být null).
     */
    private void downloadMp3Action(String videoUrl) {
        final var videoCode = preValidation(videoUrl);
        if (videoCode == null) {
            return;
        }
        invokeNewState(VideoStateType.FETCHING, new Object[]{videoCode}, false);

        new Thread(() -> {
            synchronized (LOCK) {
                var videoInfo = fetchVideoInfo(videoCode);
                if (videoInfo != null) {
                    LOG.info("Information was successfully obtained");
                    invokeNewState(VideoStateType.COLLECTING_FOR_DOWNLOAD_MP3);

                    var dialog = new FormatSelectionDialog(this, config, videoInfo, tfOutputDir.getText());
                    dialog.forceSelectMp3();
                    var selectionData = dialog.getFormatSelectionData();

                    invokeNewState(VideoStateType.CREATING_DOWNLOAD, true);
                    LOG.info("MP3 selected. Download will start");

                    var task = prepareDownloadTask(videoInfo, selectionData);
                    tm.execute(task);
                }
                invokeNewState(VideoStateType.READY);
            }
        }).start();
    }

    /**
     * Akce volaná při požadavku na stažení nejlepšího video formátu.
     *
     * @param videoUrl URL na video (může být null).
     */
    private void downloadBestVideoAction(String videoUrl) {
        final var videoCode = preValidation(videoUrl);
        if (videoCode == null) {
            return;
        }
        invokeNewState(VideoStateType.FETCHING, new Object[]{videoCode}, false);

        new Thread(() -> {
            var videoInfo = fetchVideoInfo(videoCode);
            if (videoInfo != null) {
                LOG.info("Information was successfully obtained");
                invokeNewState(VideoStateType.COLLECTING_FOR_DOWNLOAD_BEST_VIDEO);

                var dialog = new FormatSelectionDialog(this, config, videoInfo, tfOutputDir.getText());
                dialog.forceSelectBestVideo();
                var selectionData = dialog.getFormatSelectionData();

                invokeNewState(VideoStateType.CREATING_DOWNLOAD, true);
                LOG.info("Best video selected. Download will start");

                var task = prepareDownloadTask(videoInfo, selectionData);
                tm.execute(task);
            }
            invokeNewState(VideoStateType.READY);
        }).start();
    }

    /**
     * Akce, která vybere výstupní adresář.
     */
    private void selectDirectoryAction() {
        var chooser = new JFileChooser();
        chooser.setCurrentDirectory(Utils.getCurrentDirectory());
        chooser.setDialogTitle("Output directory selection");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            var path = chooser.getSelectedFile().getAbsolutePath();
            tfOutputDir.setText(path);
            LOG.info("The output directory has been changed to: {}", path);
        }
    }

    /**
     * Otevře dialog pro dávkovou operaci.
     */
    private void openBatchAction() {
        var dialog = new BatchDialog(this);
        dialog.setVisible(true);
        var videos = dialog.getVideos();
        dialog.dispose();

        for (var video : videos) {
            if (BatchData.TYPE_MP3.equals(video.getType())) {
                downloadMp3Action(video.getUrl());
            } else if (BatchData.TYPE_VIDEO.equals(video.getType())) {
                downloadBestVideoAction(video.getUrl());
            }
        }
    }

    /**
     * Nastaví výchozí výstupní adresář.
     */
    private void selectDefaultOutputDirectory() {
        try {
            tfOutputDir.setText(Utils.getCurrentDirectory().getCanonicalPath());
        } catch (IOException e) {
            LOG.error("Could not get the current path to the directory", e);
        }
    }

    /**
     * Nastaví nový stav a vše okolo toho.
     *
     * @param state
     */
    void invokeNewState(VideoStateType state) {
        invokeNewState(state, new Object[0], false);
    }

    /**
     * Nastaví nový stav a vše okolo toho.
     *
     * @param state
     * @param enabled Pokud bude True, tak se bez ohledu na stav povolí komponenty jako u READY.
     */
    void invokeNewState(VideoStateType state, boolean enabled) {
        invokeNewState(state, new Object[0], enabled);
    }

    /**
     * Nastaví nový stav a vše okolo toho.
     *
     * @param state
     * @param params
     * @param enabled Pokud bude True, tak se bez ohledu na stav povolí komponenty jako u READY.
     */
    void invokeNewState(VideoStateType state, Object[] params, boolean enabled) {
        SwingUtilities.invokeLater(() -> {
            lbState.setText(String.format(state.getFormat(), params));
            LOG.debug("State changed to: {}", state);

            switch (state) {
                case READY:
                    for (var comp : topPane.getComponents()) {
                        comp.setEnabled(true);
                    }
                    for (var comp : globalPane.getComponents()) {
                        comp.setEnabled(true);
                    }
                    btnFetch.setEnabled(true);
                    btnDownloadMp3.setEnabled(true);
                    btnDownloadBestVideo.setEnabled(true);
                    tfVideo.setText("");
                    break;

                default:
                    for (var comp : topPane.getComponents()) {
                        comp.setEnabled(enabled);
                    }
                    for (var comp : globalPane.getComponents()) {
                        comp.setEnabled(enabled);
                    }
                    btnFetch.setEnabled(enabled);
                    btnDownloadMp3.setEnabled(enabled);
                    btnDownloadBestVideo.setEnabled(enabled);
                    break;
            }
        });
    }

    /**
     * Naplní comboBox pro encoding.
     */
    private void fillEncoding() {
        for (var encoding : EncodingType.values()) {
            boxEncoding.addItem(new ComboBoxItem(encoding.getEncoding(), encoding.getLabel()));
        }
    }

    /**
     * Vytvoří panel s informacemi o videu, které se má stáhnout.
     *
     * @param task
     * @param data
     * @param videoInfo
     * @return
     */
    private ItemData createVideoItem(DownloadTask task, FormatSelectionData data, VideoInfoJson videoInfo) {
        var pane = new JPanel();
        pane.setLayout(new BorderLayout());
        pane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY),
                BorderFactory.createRaisedSoftBevelBorder()));
        pane.setBackground(Color.WHITE);
        pane.setPreferredSize(new Dimension(64, 64));
        pane.setName(String.valueOf(task.getId())); // slouží jako identifikátor každého panelu

        var imgPane = new JPanel();
        pane.add(imgPane, BorderLayout.WEST);
        imgPane.setLayout(new BorderLayout());
        imgPane.setBackground(pane.getBackground());

        var contentPane = new JPanel();
        pane.add(contentPane, BorderLayout.CENTER);
        contentPane.setLayout(new BorderLayout());
        contentPane.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
        contentPane.setBackground(pane.getBackground());

        var footerPane = new JPanel();
        contentPane.add(footerPane, BorderLayout.SOUTH);
        footerPane.setLayout(new BorderLayout());
        footerPane.setBackground(pane.getBackground());
        footerPane.setPreferredSize(new Dimension(-1, 20));

        var buttonPaneLayout = new FlowLayout(FlowLayout.RIGHT);
        buttonPaneLayout.setVgap(0);
        var buttonPane = new JPanel();
        footerPane.add(buttonPane, BorderLayout.CENTER);
        buttonPane.setLayout(buttonPaneLayout);
        buttonPane.setBackground(pane.getBackground());

        // ---

        var img = Utils.scaleImage(data.getThumbnail(), 64, 64);
        var lbImg = new JLabel();
        imgPane.add(lbImg);
        lbImg.setPreferredSize(pane.getPreferredSize());
        lbImg.setIcon(new ImageIcon(img));
        lbImg.setVerticalAlignment(JLabel.CENTER);

        var lbName = new JLabel(videoInfo.getTitle());
        contentPane.add(lbName, BorderLayout.NORTH);
        lbName.setFont(UNI_FONT.deriveFont(Font.BOLD, 14));

        var lbProgress = new JLabel("Waiting in the queue...");
        contentPane.add(lbProgress, BorderLayout.WEST);
        lbProgress.setFont(CODE_FONT.deriveFont(Font.PLAIN, 11));

        var duration = Utils.convertToReadableDuration(videoInfo.getDuration());
        var lbIndex = new JLabel(MessageFormat
                .format("#{0} | {1} | {2} | {3}", ++counter, data.getFormatType().getLabel(), duration,
                        data.getShortName()));
        footerPane.add(lbIndex, BorderLayout.WEST);
        lbIndex.setFont(CODE_FONT.deriveFont(Font.BOLD, 10));
        lbIndex.setForeground(Color.GRAY);

        var btnStop = new VideoButtonComponent("[x]", new Color(180, 50, 50));
        btnStop.setToolTipText("Stop and delete");
        btnStop.setHoverColor(new Color(220, 60, 60));
        btnStop.addActionListener(e -> {
            if (task.canBeStoppedWithoutWarning()) {
                LOG.info("Removing item with video URL: {}", videoInfo.getWebPageUrl());
                btnStop.setEnabled(false);
                task.stop();
            } else {
                var dlg = JOptionPane
                        .showConfirmDialog(this, "Are you sure you want to cancel the download?", "Cancel download",
                                JOptionPane.YES_NO_OPTION);
                if (dlg == JOptionPane.YES_OPTION) {
                    LOG.info("Removing item with video URL: {}", videoInfo.getWebPageUrl());
                    btnStop.setEnabled(false);
                    task.stop();
                }
            }
        });

        var btnCopy = new VideoButtonComponent("Copy", new Color(160, 80, 180));
        btnCopy.setToolTipText("Copies to clipboard: " + videoInfo.getWebPageUrl());
        btnCopy.setHoverColor(new Color(194, 83, 214));
        btnCopy.addActionListener(e -> {
            var clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            var selection = new StringSelection(videoInfo.getWebPageUrl());
            clipboard.setContents(selection, null);
        });

        var btnOpenBrowser = new VideoButtonComponent("Browser", new Color(50, 50, 140));
        btnOpenBrowser.setToolTipText("Open in browser");
        btnOpenBrowser.setHoverColor(new Color(82, 82, 255));
        btnOpenBrowser.addActionListener(e -> {
            var url = videoInfo.getWebPageUrl();
            LOG.debug("Opening URL '{}' in browser", url);
            if (!Utils.openBrowser(url)) {
                JOptionPane.showMessageDialog(this, "There was an error opening the URL browser: " + url, "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });

        var btnOpen = new VideoButtonComponent("Open", new Color(74, 140, 72));
        btnOpen.setToolTipText("Open in the player");
        btnOpen.setHoverColor(new Color(85, 170, 77));
        btnOpen.setVisible(false);
        btnOpen.addActionListener(e -> {
            var file = new File(data.getOutputFile());
            if (file.exists()) {
                try {
                    LOG.debug("File '{}' exists. Opening...", file.getAbsolutePath());
                    Desktop.getDesktop().open(file);
                } catch (IOException ex) {
                    LOG.error("There was an error opening the file: " + file.getAbsolutePath(), ex);
                    JOptionPane.showMessageDialog(this, "Failed to open file.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        buttonPane.add(btnOpen);
        buttonPane.add(btnOpenBrowser);
        buttonPane.add(btnCopy);
        buttonPane.add(btnStop);

        var itemData = new ItemData();
        itemData.itemPane = pane;
        itemData.lbProgress = lbProgress;
        itemData.btnOpen = btnOpen;

        return itemData;
    }

    /**
     * Vytvoří panel s informacemi o videu z historie.
     *
     * @param video Informace o videu.
     * @return
     */
    private ItemData createVideoItem(HistoryVideoConfig video) {
        var pane = new JPanel();

        var itemData = new ItemData();
        itemData.itemPane = pane;

        pane.setLayout(new BorderLayout());
        pane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY),
                BorderFactory.createRaisedSoftBevelBorder()));
        pane.setBackground(Color.WHITE);
        pane.setPreferredSize(new Dimension(64, 64));
        pane.setName(video.getVideoUrl()); // slouží jako identifikátor každého panelu

        var imgPane = new JPanel();
        pane.add(imgPane, BorderLayout.WEST);
        imgPane.setLayout(new BorderLayout());
        imgPane.setBackground(pane.getBackground());

        var contentPane = new JPanel();
        pane.add(contentPane, BorderLayout.CENTER);
        contentPane.setLayout(new BorderLayout());
        contentPane.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
        contentPane.setBackground(pane.getBackground());

        var footerPane = new JPanel();
        contentPane.add(footerPane, BorderLayout.SOUTH);
        footerPane.setLayout(new BorderLayout());
        footerPane.setBackground(pane.getBackground());
        footerPane.setPreferredSize(new Dimension(-1, 20));

        var buttonPaneLayout = new FlowLayout(FlowLayout.RIGHT);
        buttonPaneLayout.setVgap(0);
        var buttonPane = new JPanel();
        footerPane.add(buttonPane, BorderLayout.CENTER);
        buttonPane.setLayout(buttonPaneLayout);
        buttonPane.setBackground(pane.getBackground());

        // ---

        var image = Utils.convertToImage(video.getImage());
        var lbImg = new JLabel();
        imgPane.add(lbImg);
        lbImg.setPreferredSize(pane.getPreferredSize());
        lbImg.setIcon(new ImageIcon(image));
        lbImg.setVerticalAlignment(JLabel.CENTER);

        var lbName = new JLabel(video.getTitle());
        contentPane.add(lbName, BorderLayout.NORTH);
        lbName.setFont(UNI_FONT.deriveFont(Font.BOLD, 14));

        var lbProgress = new JLabel();
        contentPane.add(lbProgress, BorderLayout.WEST);
        lbProgress.setFont(CODE_FONT.deriveFont(Font.PLAIN, 11));
        if (video.isDone()) {
            setProgressToFinished(lbProgress);
        } else {
            setProgressToFailed(lbProgress);
        }
        itemData.lbProgress = lbProgress;

        var duration = Utils.convertToReadableDuration(video.getDuration());
        var lbIndex = new JLabel(MessageFormat
                .format("#{0} | {1} | {2} | {3} | history", ++counter, video.getFormatType(), duration,
                        video.getShortName()));
        footerPane.add(lbIndex, BorderLayout.WEST);
        lbIndex.setFont(CODE_FONT.deriveFont(Font.BOLD, 10));
        lbIndex.setForeground(Color.GRAY);

        var btnStop = new VideoButtonComponent("[X]", new Color(180, 50, 50));
        btnStop.setToolTipText("Delete");
        btnStop.setHoverColor(new Color(220, 60, 60));
        btnStop.addActionListener(e -> {
            setProgressToRemoving(lbProgress);
            btnStop.setEnabled(false);
            listComponent.removeItemByName(video.getVideoUrl());
            updateProgressTitle();
            history.removeVideo(video.getVideoUrl());
        });

        var btnCopy = new VideoButtonComponent("Copy", new Color(160, 80, 180));
        btnCopy.setToolTipText("Copies to clipboard: " + video.getVideoUrl());
        btnCopy.setHoverColor(new Color(194, 83, 214));
        btnCopy.addActionListener(e -> {
            var clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            var selection = new StringSelection(video.getVideoUrl());
            clipboard.setContents(selection, null);
        });

        var btnOpenBrowser = new VideoButtonComponent("Browser", new Color(50, 50, 140));
        btnOpenBrowser.setToolTipText("Open in browser");
        btnOpenBrowser.setHoverColor(new Color(82, 82, 255));
        btnOpenBrowser.addActionListener(e -> {
            var url = video.getVideoUrl();
            LOG.debug("Opening URL '{}' in browser", url);
            if (!Utils.openBrowser(url)) {
                JOptionPane.showMessageDialog(this, "There was an error opening the URL browser: " + url, "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });

        var file = new File(video.getOutput());
        if (video.isDone() && file.exists() && file.isFile()) {
            var btnOpen = new VideoButtonComponent("Open", new Color(74, 140, 72));
            btnOpen.setToolTipText("Open in the player");
            btnOpen.setHoverColor(new Color(85, 170, 77));
            btnOpen.setVisible(true);
            btnOpen.addActionListener(e -> {
                try {
                    LOG.debug("File '{}' exists. Opening...", file.getAbsolutePath());
                    Desktop.getDesktop().open(file);
                } catch (IOException ex) {
                    LOG.error("There was an error opening the file: " + file.getAbsolutePath(), ex);
                    JOptionPane.showMessageDialog(this, "Failed to open file.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            });
            buttonPane.add(btnOpen);
            itemData.btnOpen = btnOpen;

        } else if (!video.isDone()) {
            var btnSet = new VideoButtonComponent("Set", new Color(74, 140, 72));
            btnSet.setToolTipText("Set video to fetch field");
            btnSet.setHoverColor(new Color(85, 170, 77));
            btnSet.setVisible(true);
            btnSet.addActionListener(e -> {
                tfVideo.setText(video.getVideoUrl());
            });
            buttonPane.add(btnSet);
            itemData.btnSet = btnSet;
        }
        buttonPane.add(btnOpenBrowser);
        if (video.isDone()) {
            buttonPane.add(btnCopy);
        }
        buttonPane.add(btnStop);

        return itemData;
    }

    /**
     * Aktualizuje název panelu s progresem.
     */
    private void updateProgressTitle() {
        var components = listComponent.getItemsCount();
        var historyCount = 0;

        try {
            var field = listComponent.getClass().getDeclaredField("items");
            field.setAccessible(true);
            java.util.List list = (java.util.List) field.get(listComponent);

            for (var comp : list) {
                if (comp instanceof JPanel) {
                    var pane = (JPanel) comp;
                    if (history.getVideos().stream().filter(p -> p.getVideoUrl().equalsIgnoreCase(pane.getName())).count() > 0) {
                        historyCount++;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error calculating historical tasks.", e);
        }
        var border = (TitledBorder) progressPane.getBorder();
        border.setTitle(String.format("Total = %s | Waiting = %s | Active = %s | Historical = %s        ", components,
                tm.getQueueSize(), tm.getActiveTasks(), historyCount));
        progressPane.updateUI();
        LOG.debug("Progress title pane was set to {} tasks", components);
    }

    /**
     * Nastaví progress na dokončeno.
     *
     * @param lbProgress
     */
    private void setProgressToFinished(JLabel lbProgress) {
        lbProgress.setText("Done!");
        lbProgress.setFont(lbProgress.getFont().deriveFont(Font.BOLD));
        lbProgress.setForeground(new Color(4, 118, 0));
    }

    /**
     * Nastaví progress na selháno.
     *
     * @param lbProgress
     */
    private void setProgressToFailed(JLabel lbProgress) {
        lbProgress.setText("Failed!");
        lbProgress.setFont(lbProgress.getFont().deriveFont(Font.BOLD));
        lbProgress.setForeground(new Color(115, 0, 0));
    }

    /**
     * Nastaví progress na odebrání.
     *
     * @param lbProgress
     */
    private void setProgressToRemoving(JLabel lbProgress) {
        lbProgress.setText("Removing...");
        lbProgress.setFont(lbProgress.getFont().deriveFont(Font.BOLD));
        lbProgress.setForeground(new Color(163, 97, 0));

        if (config.isSoundsOn()) {
            try {
                var stream = ResourcesProvider.getInstance().load(ResourcesProvider.RES_STOPPED_SOUND);
                AudioPlayer.play(stream);
                Thread.sleep(1250); // synchronizace s přehrávanou hudbou
            } catch (InterruptedException e) {
                LOG.error("The process has been interrupted");
            }
        }
    }

    /**
     * Spustí vlákno na aktualizaci titulku.
     */
    private void startUpdateProgressThread() {
        var t = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    return;
                }
                updateProgressTitle();
            }
        });
        t.setDaemon(true);
        t.start();
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        rootPane = new JPanel();
        rootPane.setLayout(new BorderLayout(0, 0));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 1, new Insets(10, 10, 20, 10), -1, -1));
        panel1.setMinimumSize(new Dimension(464, 103));
        panel1.setPreferredSize(new Dimension(464, 110));
        rootPane.add(panel1, BorderLayout.NORTH);
        topPane = new JPanel();
        topPane.setLayout(new GridLayoutManager(2, 4, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add(topPane, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Youtube video:");
        topPane.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        Font label2Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label2.getFont());
        if (label2Font != null) label2.setFont(label2Font);
        label2.setText("Output directory:");
        topPane.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        tfOutputDir = new JTextField();
        tfOutputDir.setEditable(false);
        tfOutputDir.setEnabled(true);
        Font tfOutputDirFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, -1, tfOutputDir.getFont());
        if (tfOutputDirFont != null) tfOutputDir.setFont(tfOutputDirFont);
        topPane.add(tfOutputDir, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        btnSelectDir = new JButton();
        btnSelectDir.setBackground(new Color(-5733));
        Font btnSelectDirFont = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 11, btnSelectDir.getFont());
        if (btnSelectDirFont != null) btnSelectDir.setFont(btnSelectDirFont);
        btnSelectDir.setText("Select");
        btnSelectDir.setToolTipText("Opens the menu with the output directory selection.");
        topPane.add(btnSelectDir, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(20, -1), null, 0, false));
        tfVideo = new JTextField();
        Font tfVideoFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, -1, tfVideo.getFont());
        if (tfVideoFont != null) tfVideo.setFont(tfVideoFont);
        tfVideo.setText("");
        tfVideo.setToolTipText("URL or YouTube video code.");
        topPane.add(tfVideo, new GridConstraints(0, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        btnGoDirectory = new JButton();
        btnGoDirectory.setBackground(new Color(-5733));
        Font btnGoDirectoryFont = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 11, btnGoDirectory.getFont());
        if (btnGoDirectoryFont != null) btnGoDirectory.setFont(btnGoDirectoryFont);
        btnGoDirectory.setText("Open");
        btnGoDirectory.setToolTipText("Opens the output directory.");
        topPane.add(btnGoDirectory, new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(20, -1), null, 0, false));
        btnBatch = new JButton();
        btnBatch.setActionCommand("Batch");
        btnBatch.setBackground(new Color(-21018));
        btnBatch.setBorderPainted(false);
        Font btnBatchFont = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 11, btnBatch.getFont());
        if (btnBatchFont != null) btnBatch.setFont(btnBatchFont);
        btnBatch.setText("Batch");
        btnBatch.setToolTipText("Opens a window with the definition of multiple videos to be processed as a batch.");
        topPane.add(btnBatch, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(20, -1), null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(4, 1, new Insets(0, 10, 10, 0), -1, -1));
        panel2.setPreferredSize(new Dimension(350, 57));
        rootPane.add(panel2, BorderLayout.WEST);
        globalPane = new JPanel();
        globalPane.setLayout(new GridLayoutManager(5, 2, new Insets(10, 10, 10, 10), -1, -1));
        Font globalPaneFont = this.$$$getFont$$$("DejaVu Sans", -1, -1, globalPane.getFont());
        if (globalPaneFont != null) globalPane.setFont(globalPaneFont);
        panel2.add(globalPane, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        globalPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Global Settings", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setText("Encoding:");
        globalPane.add(label3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        globalPane.add(spacer1, new GridConstraints(1, 0, 4, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        boxEncoding = new JComboBox();
        Font boxEncodingFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, -1, boxEncoding.getFont());
        if (boxEncodingFont != null) boxEncoding.setFont(boxEncodingFont);
        boxEncoding.setToolTipText("Forces the use of encode.");
        globalPane.add(boxEncoding, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cbUseHttp = new JCheckBox();
        Font cbUseHttpFont = this.$$$getFont$$$("DejaVu Sans", -1, -1, cbUseHttp.getFont());
        if (cbUseHttpFont != null) cbUseHttp.setFont(cbUseHttpFont);
        cbUseHttp.setSelected(true);
        cbUseHttp.setText("Use HTTP");
        cbUseHttp.setToolTipText("HTTP will be used to download the file.");
        globalPane.add(cbUseHttp, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cbDownloadThumb = new JCheckBox();
        Font cbDownloadThumbFont = this.$$$getFont$$$("DejaVu Sans", -1, -1, cbDownloadThumb.getFont());
        if (cbDownloadThumbFont != null) cbDownloadThumb.setFont(cbDownloadThumbFont);
        cbDownloadThumb.setSelected(false);
        cbDownloadThumb.setText("Download Thumbnail");
        cbDownloadThumb.setToolTipText("If enabled, it will download a video preview image.");
        globalPane.add(cbDownloadThumb, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cbAddMetadata = new JCheckBox();
        Font cbAddMetadataFont = this.$$$getFont$$$("DejaVu Sans", -1, -1, cbAddMetadata.getFont());
        if (cbAddMetadataFont != null) cbAddMetadata.setFont(cbAddMetadataFont);
        cbAddMetadata.setSelected(true);
        cbAddMetadata.setText("Add Metadata");
        cbAddMetadata.setToolTipText("Add metadata to video / music");
        globalPane.add(cbAddMetadata, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel2.add(spacer2, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel2.add(panel3, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        btnFetch = new JButton();
        btnFetch.setBackground(new Color(-6225992));
        Font btnFetchFont = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 16, btnFetch.getFont());
        if (btnFetchFont != null) btnFetch.setFont(btnFetchFont);
        btnFetch.setText(" » Fetch «");
        btnFetch.setToolTipText("Gets video information and lets you select the output format to download.");
        panel3.add(btnFetch, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(-1, 40), null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 2, new Insets(10, 0, 0, 0), -1, -1));
        panel2.add(panel4, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        btnDownloadMp3 = new JButton();
        btnDownloadMp3.setBackground(new Color(-4998401));
        Font btnDownloadMp3Font = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 11, btnDownloadMp3.getFont());
        if (btnDownloadMp3Font != null) btnDownloadMp3.setFont(btnDownloadMp3Font);
        btnDownloadMp3.setText("Get mp3");
        btnDownloadMp3.setToolTipText("Download only audio in mp3 format.");
        panel4.add(btnDownloadMp3, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        btnDownloadBestVideo = new JButton();
        btnDownloadBestVideo.setBackground(new Color(-5649153));
        Font btnDownloadBestVideoFont = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 11, btnDownloadBestVideo.getFont());
        if (btnDownloadBestVideoFont != null) btnDownloadBestVideo.setFont(btnDownloadBestVideoFont);
        btnDownloadBestVideo.setText("Get the best video");
        btnDownloadBestVideo.setToolTipText("Download the best video format.");
        panel4.add(btnDownloadBestVideo, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(1, 1, new Insets(0, 10, 5, 10), -1, -1));
        rootPane.add(panel5, BorderLayout.SOUTH);
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new BorderLayout(0, 0));
        panel5.add(panel6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        lbState = new JLabel();
        Font lbStateFont = this.$$$getFont$$$("DejaVu Sans Mono", Font.BOLD, -1, lbState.getFont());
        if (lbStateFont != null) lbState.setFont(lbStateFont);
        lbState.setForeground(new Color(-16764761));
        lbState.setText("");
        panel6.add(lbState, BorderLayout.WEST);
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new BorderLayout(0, 0));
        panel6.add(panel7, BorderLayout.EAST);
        lbCopy = new JLabel();
        Font lbCopyFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, 10, lbCopy.getFont());
        if (lbCopyFont != null) lbCopy.setFont(lbCopyFont);
        lbCopy.setText("by mimic | 2019 | v0.0.0");
        panel7.add(lbCopy, BorderLayout.CENTER);
        btnDonate = new JButton();
        btnDonate.setBorderPainted(true);
        btnDonate.setHorizontalTextPosition(0);
        btnDonate.setText("Donate");
        panel7.add(btnDonate, BorderLayout.EAST);
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 10, 10), -1, -1));
        rootPane.add(panel8, BorderLayout.CENTER);
        progressPane = new JPanel();
        progressPane.setLayout(new BorderLayout(0, 0));
        Font progressPaneFont = this.$$$getFont$$$("DejaVu Sans", -1, -1, progressPane.getFont());
        if (progressPaneFont != null) progressPane.setFont(progressPaneFont);
        panel8.add(progressPane, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 1, false));
        progressPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Progress (0)", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        Font font = new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
        boolean isMac = System.getProperty("os.name", "").toLowerCase(Locale.ENGLISH).startsWith("mac");
        Font fontWithFallback = isMac ? new Font(font.getFamily(), font.getStyle(), font.getSize()) : new StyleContext().getFont(font.getFamily(), font.getStyle(), font.getSize());
        return fontWithFallback instanceof FontUIResource ? fontWithFallback : new FontUIResource(fontWithFallback);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPane;
    }

    /**
     * Objekt, který reprezentuje jednu položku videa v seznamu.
     */
    private static final class ItemData {

        private JPanel itemPane;
        private JLabel lbProgress;
        private JButton btnOpen;
        private JButton btnSet;
    }

    /**
     * Handler pro zachycení událostí nad stahováním videi.
     */
    private final class DownloadHandler implements ITaskListener, IDownloadTaskListener {

        private final ItemData itemData;

        /**
         * @param itemData
         */
        private DownloadHandler(ItemData itemData) {
            this.itemData = itemData;
        }

        @Override
        public void downloadProgress(DownloadTask task, String progress) {
            itemData.lbProgress.setText(progress);
        }

        @Override
        public void downloadFailed(DownloadTask task, Exception e) {
            if (config.isSoundsOn()) {
                var stream = ResourcesProvider.getInstance().load(ResourcesProvider.RES_FAILED_SOUND);
                AudioPlayer.play(stream);
            }
            setProgressToFailed(itemData.lbProgress);
            updateProgressTitle();
        }

        @Override
        public void forcedTermination(DownloadTask task) {
            var t = new Thread(() -> {
                setProgressToRemoving(itemData.lbProgress);
                listComponent.removeItemByName(String.valueOf(task.getId()));
                history.removeVideo(task.getConfig().getUrlOrVideoHash());
                updateProgressTitle();
                task.removeListener(this);
                task.removeDownloadListener(this);
            });
            t.setDaemon(true);
            t.start();
        }

        @Override
        public void started(Task task) {
            itemData.lbProgress.setText("Starting...");
            updateProgressTitle(); // bez delay
        }

        @Override
        public void finished(Task task, boolean success) {
            if (success) {
                setProgressToFinished(itemData.lbProgress);
                itemData.btnOpen.setVisible(true);

                task.removeListener(this);

                if (task instanceof DownloadTask) {
                    var downTask = (DownloadTask) task;
                    history.setDone(downTask.getConfig().getUrlOrVideoHash());
                }
                LOG.debug("{} Task finished. Removing listener", task.getId());

                if (config.isSoundsOn()) {
                    var stream = ResourcesProvider.getInstance().load(ResourcesProvider.RES_DOWNLOADED_SOUND);
                    AudioPlayer.play(stream);
                }
                updateProgressTitle();
            }
        }
    }
}
