package oc.mimic.ytdl.gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import oc.mimic.ytdl.ResourcesProvider;
import oc.mimic.ytdl.config.ApplicationConfig;
import oc.mimic.ytdl.gui.dto.ComboBoxFormatItem;
import oc.mimic.ytdl.gui.dto.FormatSelectionData;
import oc.mimic.ytdl.gui.dto.VideoStateType;
import oc.mimic.ytdl.logic.Utils;
import oc.mimic.ytdl.logic.json.LongestType;
import oc.mimic.ytdl.logic.json.LongestTypeHelper;
import oc.mimic.ytdl.logic.json.VideoFormatJson;
import oc.mimic.ytdl.logic.json.VideoInfoJson;
import oc.mimic.ytdl.logic.types.CustomAudioType;
import oc.mimic.ytdl.logic.types.CustomVideoType;
import oc.mimic.ytdl.logic.types.FormatType;
import oc.mimic.ytdl.logic.types.OutputFileSuffixType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.nio.file.Paths;
import java.util.List;
import java.util.*;

/**
 * Dialogové okno s výběrem formátu ke stažení.
 *
 * @author mimic
 */
final class FormatSelectionDialog extends JDialog {

    private static final Logger LOG = LoggerFactory.getLogger(FormatSelectionDialog.class);

    private static final long serialVersionUID = -5605339723860943154L;
    public static final String THE_BEST_PREFIX = "The best - ";

    private static final int MAX_RATING = 5;
    private static final int FILESIZE_LENGTH = 10;
    private static final String BOX_NAME_AUDIO = "audio";
    private static final String BOX_NAME_VIDEO = "video";
    private static final String BOX_NAME_MOBILE = "mobile";

    private JPanel rootPane;
    private JComboBox<ComboBoxFormatItem> boxAudio;
    private JComboBox<ComboBoxFormatItem> boxVideo;
    private JComboBox<ComboBoxFormatItem> boxMobile;
    private JButton btnDownload;
    private JButton btnUrl;
    private JRadioButton radioAudio;
    private JRadioButton radioVideo;
    private JRadioButton radioMobile;
    private JLabel lbThumbnail;
    private JLabel lbLikeDislike;
    private JLabel lbDuration;
    private JTextArea lbName;
    private JTextArea lbUrl;
    private JTextArea lbOutputFile;

    private final Map<FormatType, Stack<ComboBoxFormatItem>> formats;
    private VideoFormatJson bestAudio;
    private VideoFormatJson bestVideo;

    private final ApplicationConfig config;
    private final VideoInfoJson videoInfo;
    private final String outputDirectory;
    private ComboBoxFormatItem selectedItem;
    private FormatSelectionData formatSelectionData;
    private final Image videoThumbnail;

    /**
     * @param parent
     * @param config
     * @param videoInfo
     * @param outputDirectory
     */
    FormatSelectionDialog(MainForm parent, ApplicationConfig config, VideoInfoJson videoInfo, String outputDirectory) {
        super();
        this.config = config;
        this.videoInfo = videoInfo;
        this.outputDirectory = outputDirectory;

        LOG.debug("Loading a modal window with a downloadable format selection");

        setTitle("Format selection...");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        setModal(true);

        var image = ResourcesProvider.getInstance().getImage(ResourcesProvider.RES_ICON);
        setIconImage(image);

        setContentPane(rootPane);
        updateFonts();

        // připraví stacky pro formáty
        formats = new HashMap<>();
        formats.put(FormatType.AUDIO, new Stack<>());
        formats.put(FormatType.VIDEO, new Stack<>());
        formats.put(FormatType.MOBILE, new Stack<>());

        // umožní vybrat pouze jeden radio
        var radioGroup = new ButtonGroup();
        radioGroup.add(radioAudio);
        radioGroup.add(radioVideo);
        radioGroup.add(radioMobile);

        // asociace mezi radio a comboBox (když se klikne na radio)
        for (var it = radioGroup.getElements().asIterator(); it.hasNext(); ) {
            var radio = it.next();
            radio.addActionListener((e) -> boxActivationAction(radio));
        }
        // naplnění dialogu daty
        filterAndCategorizeFormats();
        fillAudioComboBox();
        fillVideoComboBox();
        fillMobileComboBox();

        // udělá výchozí výběr
        var selectedRadio = radioVideo; // definuje výchozí výběr
        selectedRadio.setSelected(true);
        boxActivationAction(selectedRadio);

        pack(); // je nutné, aby se prvně nastavila velikost okna, než se zahájí výpočet pro obrázek

        videoThumbnail = Utils.readImage(videoInfo.getThumbnailUrl());
        fillOthers(videoThumbnail);

        pack(); // opět je nutné, protože se obrázek někdy nemusí načíst, tak aby se nastavila správná velikost okna
        setLocationRelativeTo(parent);

        // další nastavení
        btnUrl.setCursor(MainForm.HAND_CURSOR);
        btnDownload.setCursor(MainForm.HAND_CURSOR);
        boxAudio.setCursor(MainForm.HAND_CURSOR);
        boxVideo.setCursor(MainForm.HAND_CURSOR);
        boxMobile.setCursor(MainForm.HAND_CURSOR);

        // události
        boxAudio.addActionListener(e -> boxChanged(boxAudio));
        boxVideo.addActionListener(e -> boxChanged(boxVideo));
        boxMobile.addActionListener(e -> boxChanged(boxMobile));
        btnDownload.addActionListener((e) -> downloadAction());

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                parent.invokeNewState(VideoStateType.WAITING_FOR_FORMAT_SELECTION);
            }
        });
        LOG.debug("Loaded");
    }

    /**
     * Získá vybraný formát.
     *
     * @return
     */
    FormatSelectionData getFormatSelectionData() {
        return formatSelectionData;
    }

    /**
     * Automaticky vybere mp3 formát.
     */
    void forceSelectMp3() {
        for (var i = 0; i < boxAudio.getItemCount(); i++) {
            var item = (ComboBoxFormatItem) boxAudio.getItemAt(i);

            if (item.getFormat() == null) {
                var type = CustomAudioType.getById((int) item.getKey());

                if (type == CustomAudioType.MP3) {
                    boxAudio.setSelectedItem(item);
                    break;
                }
            }
        }
        downloadAction();
    }

    /**
     * Automaticky vybere nejlepší video formát.
     */
    void forceSelectBestVideo() {
        var max = FormatSelectionDialog.THE_BEST_PREFIX + config.getMaxBatchVideoQuality();
        var i = 0;
        Object selected = null;

        for (; i < boxVideo.getItemCount(); i++) {
            var box = boxVideo.getItemAt(i);

            if (box.getLabel().startsWith(FormatSelectionDialog.THE_BEST_PREFIX)) {
                if (box.getLabel().equals(max)) {
                    selected = box;
                    break;
                }
            } else {
                break;
            }
        }
        if (selected == null) {
            for (; i < boxVideo.getItemCount(); i++) {
                var box = boxVideo.getItemAt(i);

                // přeskočíme avi, protože se bere z nejlepší kvality a ještě probíhá konverze
                if (box.getLabel().startsWith("avi -")) {
                    continue;
                }
                selected = box;
                break;
            }
        }
        if (selected == null) {
            throw new RuntimeException("No suitable video format found.");
        }
        // první položka v comboBoxu je nejlepší formát
        boxVideo.setSelectedItem(selected);
        downloadAction();
    }

    /**
     * Logika po kliknutí na nějaké radio.
     *
     * @param button
     */
    private void boxActivationAction(AbstractButton button) {
        for (var comp : rootPane.getComponents()) {
            if (comp instanceof JComboBox) {
                var check = comp.getName().equals(button.getName());
                // pomocí asociace jména na radio a comboBox (jména musí byt stejná) se změní stav enabled, aby bylo
                // možné pro konkrétní radio vybrat pouze z jednoho comboBox
                comp.setEnabled(check);

                if (check) {
                    // i při změně radia je potřeba zavolat událost, aby se změnila přípona výstupního souboru
                    boxChanged((JComboBox) comp);
                }
            }
        }
    }

    /**
     * Hodnota v nějakém comboBoxu se změnila. Volá se i z události.
     *
     * @param comboBox
     */
    private void boxChanged(JComboBox comboBox) {
        selectedItem = (ComboBoxFormatItem) comboBox.getSelectedItem();
        var file = createOutputFile(comboBox);
        lbOutputFile.setText(file);
        LOG.debug("Selection changed to: {} = {}", selectedItem.getKey(), selectedItem.getLabel());
    }

    /**
     * Kliknutí na tlačítko download.
     */
    private void downloadAction() {
        var format = selectedItem.getFormat();
        CustomAudioType customAudioType = null;
        CustomVideoType customVideoType = null;

        // formát může být null v případě, že se jedná o vlastní typ, který nebyl vrácen z youtube-dl
        if (format == null) {
            var formatId = (int) selectedItem.getKey();
            // vždy bude jeden vlastní typ null
            customAudioType = CustomAudioType.getById(formatId);
            customVideoType = CustomVideoType.getById(formatId);

            if (customAudioType == null && customVideoType == null) {
                LOG.error("An unexpected error occurred when selecting the correct format. The format ID '{}' was" +
                        " not found or is invalid", formatId);
                JOptionPane.showMessageDialog(this, "Invalid format. More information in log file.", "Error",
                        JOptionPane.ERROR_MESSAGE);
            } else if (customAudioType != null) {
                format = bestAudio;
            } else if (customVideoType != null) {
                format = bestVideo;
            }
        }
        // byl vybrán správný formát
        if (format != null) {
            LOG.debug("Selected '{}' format ID: {}, custom audio: {}", selectedItem.getFormatType(), format.getFormatId(),
                    customAudioType);

            formatSelectionData = new FormatSelectionData(selectedItem.getFormatType(), format);
            formatSelectionData.setShortName(selectedItem.getShortName());
            formatSelectionData.setCustomAudioType(customAudioType);
            formatSelectionData.setCustomVideoType(customVideoType);
            formatSelectionData.setOutputFile(lbOutputFile.getText());
            formatSelectionData.setThumbnail(videoThumbnail);

            dispose();
        }
    }

    /**
     * Odfiltruje formáty do patřičných kategorii.
     */
    private void filterAndCategorizeFormats() {
        // názvy budou doplněny později (proto jsou null)
        for (var f : videoInfo.getFormats()) {
            if (f.getFormat().contains("audio only")) {
                var stack = formats.get(FormatType.AUDIO);
                stack.add(new ComboBoxFormatItem(FormatType.AUDIO, f, f.getFormatId(), null));
                LOG.trace("Audio: {} = {}", f.getFormatId(), f.getFormat());

            } else if (f.getFormatId() < 100) { // dle všeho mají mobilní formáty vždy < 100
                var stack = formats.get(FormatType.MOBILE);
                stack.add(new ComboBoxFormatItem(FormatType.MOBILE, f, f.getFormatId(), null));
                LOG.trace("Mobile: {} = {}", f.getFormatId(), f.getFormat());
            } else {
                var stack = formats.get(FormatType.VIDEO);
                stack.add(new ComboBoxFormatItem(FormatType.VIDEO, f, f.getFormatId(), null));
                LOG.trace("Video: {} = {}", f.getFormatId(), f.getFormat());
            }
        }
    }

    /**
     * Vytvoří vlastní audio formáty, které se vytvoří z nejlepšího formátu.
     *
     * @param audioFormats
     * @return
     */
    private List<ComboBoxFormatItem> createCustomAudioFormats(List<VideoFormatJson> audioFormats) {
        var customAudios = new ArrayList<ComboBoxFormatItem>();
        var usedAudioExtensions = new ArrayList<String>();

        for (var f : audioFormats) {
            usedAudioExtensions.add(f.getExtension().toLowerCase());
        }
        for (var type : CustomAudioType.values()) {
            // pokud se vlastní typ již používá nativně, tak se přeskočí
            if (!usedAudioExtensions.contains(type.getValue())) {
                var label = String
                        .format("%s - (convert from The best)", Utils.lPad(type.getLabel(), CustomAudioType.getLongestLabel()));
                var item = new ComboBoxFormatItem(FormatType.AUDIO, null, type.getId(), label);
                item.setShortName(type.getNewValue() + " (custom)");
                customAudios.add(item);
            }
        }
        return customAudios;
    }

    /**
     * Vytvoří vlastní video formáty, které se vytvoří z nejlepšího formátu.
     *
     * @param videoFormats
     * @return
     */
    private List<ComboBoxFormatItem> createCustomVideoFormats(List<VideoFormatJson> videoFormats) {
        var customVideos = new ArrayList<ComboBoxFormatItem>();
        var usedVideoExtensions = new ArrayList<String>();

        for (var f : videoFormats) {
            usedVideoExtensions.add(f.getExtension().toLowerCase());
        }
        for (var type : CustomVideoType.values()) {
            // pokud se vlastní typ již používá nativně, tak se přeskočí
            if (!usedVideoExtensions.contains(type.getExtension())) {
                var label = String
                        .format("%s - (convert from The best)", Utils.lPad(type.getLabel(), CustomVideoType.getLongestLabel()));
                var item = new ComboBoxFormatItem(FormatType.VIDEO, null, type.getId(), label);
                item.setShortName(type.getExtension() + " (custom)");
                customVideos.add(item);
            }
        }
        return customVideos;
    }

    /**
     * Vybere nejlepší formáty audia a vytvoří z nich položky do comboBox.
     *
     * @param audioFormats
     * @return
     */
    private List<ComboBoxFormatItem> findBestAudios(List<VideoFormatJson> audioFormats) {
        var bestAudios = new ArrayList<ComboBoxFormatItem>();

        if (!audioFormats.isEmpty()) {
            var f = audioFormats.get(audioFormats.size() - 1); // poslední prvek je vždy nejlepší
            var longestExtension = LongestTypeHelper.calculate(LongestType.EXTENSION, audioFormats);
            var label = THE_BEST_PREFIX + Utils.rPad(f.getExtension(), longestExtension);
            var item = new ComboBoxFormatItem(FormatType.AUDIO, f, f.getFormatId(), label);
            item.setSuffix(OutputFileSuffixType.BEST);
            item.setShortName(f.getExtension() + " (best)");

            bestAudios.add(item);
            bestAudio = f;
        }
        return bestAudios;
    }

    /**
     * Vybere nejlepší formáty videi a vytvoří z nich položky do comboBox.
     *
     * @param videoFormats
     * @return
     */
    private List<ComboBoxFormatItem> findBestVideos(List<VideoFormatJson> videoFormats) {
        var bestVideos = new ArrayList<ComboBoxFormatItem>(4);
        var set2160p = false;
        var set1440p = false;
        var set1080p = false;
        var set720p = false;

        // nejlepší formát videa je na konci
        bestVideo = videoFormats.get(videoFormats.size() - 1);

        // je potřeba brát odzadu, protože nejlepší jsou vždy na konci
        for (var i = videoFormats.size() - 1; i >= 0; --i) {
            var f = videoFormats.get(i);
            // zde je 5, protože již známe maxímální délku textu
            var label = THE_BEST_PREFIX + Utils.rPad(f.getFormatNote(), 5);

            if (!set2160p && f.getFormatNote().equalsIgnoreCase("2160p")) {
                var item = new ComboBoxFormatItem(FormatType.VIDEO, f, f.getFormatId(), label);
                item.setSuffix(OutputFileSuffixType.P2160);
                item.setShortName(f.getFormatNote() + " (best)");
                bestVideos.add(item);
                set2160p = true;
            }
            if (!set1440p && f.getFormatNote().equalsIgnoreCase("1440p")) {
                var item = new ComboBoxFormatItem(FormatType.VIDEO, f, f.getFormatId(), label);
                item.setSuffix(OutputFileSuffixType.P1440);
                item.setShortName(f.getFormatNote() + " (best)");
                bestVideos.add(item);
                set1440p = true;
            }
            if (!set1080p && f.getFormatNote().equalsIgnoreCase("1080p")) {
                var item = new ComboBoxFormatItem(FormatType.VIDEO, f, f.getFormatId(), label);
                item.setSuffix(OutputFileSuffixType.P1080);
                item.setShortName(f.getFormatNote() + " (best)");
                bestVideos.add(item);
                set1080p = true;
            }
            if (!set720p && f.getFormatNote().equalsIgnoreCase("720p")) {
                var item = new ComboBoxFormatItem(FormatType.VIDEO, f, f.getFormatId(), label);
                item.setSuffix(OutputFileSuffixType.P720);
                item.setShortName(f.getFormatNote() + " (best)");
                bestVideos.add(item);
                set720p = true;
            }
        }
        return bestVideos;
    }

    /**
     * Vybere nejlepší formáty mobilních videi a vytvoří z nich položky do comboBox.
     *
     * @param mobileFormats
     * @return
     */
    private List<ComboBoxFormatItem> findBestMobiles(List<VideoFormatJson> mobileFormats) {
        var bestMobiles = new ArrayList<ComboBoxFormatItem>();

        if (!mobileFormats.isEmpty()) {
            var f = mobileFormats.get(mobileFormats.size() - 1);
            var longestFormatNote = LongestTypeHelper.calculate(LongestType.FORMAT_NOTE, mobileFormats);
            var label = THE_BEST_PREFIX + Utils.rPad(f.getFormatNote(), longestFormatNote);
            var item = new ComboBoxFormatItem(FormatType.MOBILE, f, f.getFormatId(), label);
            item.setSuffix(OutputFileSuffixType.BEST);
            item.setShortName(f.getFormatNote() + " (best)");

            bestMobiles.add(item);
        }
        return bestMobiles;
    }

    /**
     * Naplní comboBox pro audio.
     */
    private void fillAudioComboBox() {
        var stack = formats.get(FormatType.AUDIO);
        var audioFormats = new ArrayList<VideoFormatJson>(stack.size());

        for (var item : stack) {
            audioFormats.add(item.getFormat());
        }
        var longestAudioExtension = LongestTypeHelper.calculate(LongestType.EXTENSION, audioFormats);
        var longestAudioCodec = LongestTypeHelper.calculate(LongestType.AUDIO_CODEC, audioFormats);

        var bestAudios = findBestAudios(audioFormats);
        for (var item : bestAudios) {
            boxAudio.addItem(item);
        }
        var customAudioFormats = createCustomAudioFormats(audioFormats);
        for (var item : customAudioFormats) {
            boxAudio.addItem(item);
        }
        for (var item : stack) {
            var f = item.getFormat();

            if (f != null) {
                var filesize = Utils.humanReadableFileSize(f.getFilesize(), true);
                var label = String.format("%s - %s kbps - (%s) | %s", Utils.lPad(f.getExtension(), longestAudioExtension),
                        Utils.rPad(String.valueOf(f.getAvarageBitrate()), 3), Utils.lPad(f.getAudioCodec(), longestAudioCodec),
                        Utils.rPad(filesize, FILESIZE_LENGTH));
                item.setLabel(label);
                item.setShortName(String.format("%s - %s kbps", f.getExtension(), f.getAvarageBitrate()));
            }
        }
        while (!stack.isEmpty()) {
            var item = stack.pop();
            boxAudio.addItem(item);
        }
    }

    /**
     * Naplní comboBox pro video.
     */
    private void fillVideoComboBox() {
        var stack = formats.get(FormatType.VIDEO);
        var videoFormats = new ArrayList<VideoFormatJson>(stack.size());

        for (var item : stack) {
            videoFormats.add(item.getFormat());
        }
        var longestVideoFps = LongestTypeHelper.calculate(LongestType.FPS, videoFormats);
        var longestVideoBetterExtension = LongestTypeHelper.calculate(LongestType.BETTER_EXTENSION, videoFormats);
        var longestVideoFormatNote = LongestTypeHelper.calculate(LongestType.FORMAT_NOTE, videoFormats);
        var longestVideoCodec = LongestTypeHelper.calculate(LongestType.VIDEO_CODEC, videoFormats);

        var bestVideos = findBestVideos(videoFormats);
        for (var item : bestVideos) {
            boxVideo.addItem(item);
        }
        var customVideoFormats = createCustomVideoFormats(videoFormats);
        for (var item : customVideoFormats) {
            boxVideo.addItem(item);
        }
        for (var item : stack) {
            var f = item.getFormat();

            if (f != null && item.getLabel() == null) {
                var filesize = Utils.humanReadableFileSize(f.getFilesize(), true);
                var label = String
                        .format("%s - %s - (%s | %s fps | %s) | %s", Utils.lPad(f.getFormatNote(), longestVideoFormatNote),
                                Utils.lPad(f.getBetterExtension(), longestVideoBetterExtension),
                                Utils.rPad(f.getWidth() + "x" + f.getHeight(), 9),
                                Utils.lPad(String.valueOf(f.getFps()), longestVideoFps),
                                Utils.lPad(f.getVideoCodec(), longestVideoCodec), Utils.rPad(filesize, FILESIZE_LENGTH));
                item.setLabel(label);
                item.setShortName(String.format("%s - %s - %sx%s", f.getBetterExtension(), f.getFormatNote(), f.getWidth(),
                        f.getHeight()));
            }
        }
        while (!stack.isEmpty()) {
            var item = stack.pop();
            boxVideo.addItem(item);
        }
    }

    /**
     * Naplní comboBox pro mobilní videa.
     */
    private void fillMobileComboBox() {
        var stack = formats.get(FormatType.MOBILE);
        var mobileFormats = new ArrayList<VideoFormatJson>(stack.size());

        for (var item : stack) {
            mobileFormats.add(item.getFormat());
        }
        var longestMobileBetterExtension = LongestTypeHelper.calculate(LongestType.BETTER_EXTENSION, mobileFormats);
        var longestMobileFormatNote = LongestTypeHelper.calculate(LongestType.FORMAT_NOTE, mobileFormats);
        var longestVideoCodec = LongestTypeHelper.calculate(LongestType.VIDEO_CODEC, mobileFormats);

        var bestMobiles = findBestMobiles(mobileFormats);
        for (var item : bestMobiles) {
            boxMobile.addItem(item);
        }
        for (var item : stack) {
            var f = item.getFormat();
            var filesize = Utils.humanReadableFileSize(f.getFilesize(), true);
            var label = String.format("%s - %s - (%s | %s) | %s", Utils.lPad(f.getFormatNote(), longestMobileFormatNote),
                    Utils.lPad(f.getBetterExtension(), longestMobileBetterExtension),
                    Utils.rPad(f.getWidth() + "x" + f.getHeight(), 9), Utils.lPad(f.getVideoCodec(), longestVideoCodec),
                    Utils.rPad(filesize, FILESIZE_LENGTH));
            item.setLabel(label);
            item.setShortName(
                    String.format("%s - %s - %sx%s", f.getBetterExtension(), f.getFormatNote(), f.getWidth(), f.getHeight()));
        }
        while (!stack.isEmpty()) {
            var item = stack.pop();
            boxMobile.addItem(item);
        }
    }

    /**
     * Vyplní všechny ostatní hodnoty v okně.
     *
     * @param videoImage
     */
    private void fillOthers(Image videoImage) {
        // nastaví náhled obrázku
        if (videoImage != null) {
            var image = Utils.scaleImage(videoImage, lbOutputFile.getWidth(), lbOutputFile.getWidth());
            lbThumbnail.setIcon(new ImageIcon(image));
            lbThumbnail.setBorder(LineBorder.createBlackLineBorder());
        } else {
            lbThumbnail.setText("-");
        }
        // výstupní soubor
        lbOutputFile.setBorder(BorderFactory.createEmptyBorder());
        lbOutputFile.setBackground(new Color(0, 0, 0, 0));
        // URL videa
        lbUrl.setBorder(BorderFactory.createEmptyBorder());
        lbUrl.setBackground(new Color(0, 0, 0, 0));
        lbUrl.setText(videoInfo.getWebPageUrl());
        btnUrl.addActionListener((e) -> goToVideoLink(lbUrl.getText()));
        // název / titulek
        lbName.setBorder(BorderFactory.createEmptyBorder());
        lbName.setBackground(new Color(0, 0, 0, 0));
        lbName.setText(videoInfo.getTitle());
        // like / dislike a ratio
        var percent = (100. / MAX_RATING) * videoInfo.getAverageRating();
        lbLikeDislike.setText(String.format(
                "<html><font color='#006600'>%s</font> / <font color='#bb0000'>%s</font> (rating: <font " +
                        "color='#000000'>%.02f%%</font>)</html>", videoInfo.getLikeCount(), videoInfo.getDislikeCount(), percent));
        // délka videa
        lbDuration.setText(Utils.convertToReadableDuration(videoInfo.getDuration()));
    }

    /**
     * Vytvoří cestu k budoucímu souboru.
     *
     * @param comboBox
     * @return
     */
    private String createOutputFile(JComboBox comboBox) {
        var outputDir = outputDirectory.replaceAll("\\\\", "/");
        if (!outputDir.endsWith("/")) {
            outputDir += "/";
        }
        var selectedItem = (ComboBoxFormatItem) comboBox.getSelectedItem();
        var format = selectedItem.getFormat();
        String extension = "";
        String subdir = "";

        // upraví název souboru, aby neobsahoval neplatné znaky
        var videoTitle = videoInfo.getTitle();
        videoTitle = videoTitle.replaceAll("[\\\\/:*?\"<>|]", " ");

        if (selectedItem.getSuffix() != null) {
            videoTitle += "." + selectedItem.getSuffix().getSuffix();
        }
        switch (comboBox.getName().toLowerCase()) {
            case BOX_NAME_AUDIO:
                if (format == null) { // vlastní formáty audia (např. mp3) nemají vyplněný (ani nemůžou) formát
                    extension = CustomAudioType.getById((int) selectedItem.getKey()).getNewValue();
                } else {
                    extension = format.getExtension();
                }
                subdir = "audio";
                break;

            case BOX_NAME_VIDEO:
                if (format == null) { // vlastní formáty videa (např. avi) nemají vyplněný (ani nemůžou) formát
                    extension = CustomVideoType.getById((int) selectedItem.getKey()).getExtension();
                } else {
                    extension = format.getBetterExtension();
                }
                subdir = "video";
                break;

            case BOX_NAME_MOBILE:
                extension = format.getBetterExtension();
                subdir = "mobile";
                break;
        }
        extension = "." + extension;
        return Paths.get(outputDir, subdir, videoTitle + extension).toString();
    }

    /**
     * Aktualizuje fonty.
     */
    private void updateFonts() {
        var uniFont = ResourcesProvider.getInstance().getFont(ResourcesProvider.RES_UNI_FONT);
        lbOutputFile.setFont(uniFont.deriveFont(Font.PLAIN, 13));
        lbName.setFont(uniFont.deriveFont(Font.PLAIN, 14));
    }

    /**
     * Otevře prohlížeč s URL. Akce po kliknutí na URL.
     *
     * @param url URL na video.
     */
    private void goToVideoLink(String url) {
        if (!Utils.openBrowser(url)) {
            JOptionPane.showMessageDialog(this, "There was an error opening the URL browser: " + url, "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        rootPane = new JPanel();
        rootPane.setLayout(new GridLayoutManager(11, 3, new Insets(20, 20, 20, 20), -1, -1));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setHorizontalAlignment(10);
        label1.setHorizontalTextPosition(10);
        label1.setText("Audio:");
        label1.setVerticalAlignment(0);
        label1.setVerticalTextPosition(0);
        rootPane.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        Font label2Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label2.getFont());
        if (label2Font != null) label2.setFont(label2Font);
        label2.setHorizontalAlignment(10);
        label2.setHorizontalTextPosition(10);
        label2.setText("Video:");
        label2.setVerticalAlignment(0);
        label2.setVerticalTextPosition(0);
        rootPane.add(label2, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setHorizontalAlignment(10);
        label3.setHorizontalTextPosition(10);
        label3.setText("Mobile:");
        label3.setVerticalAlignment(0);
        label3.setVerticalTextPosition(0);
        rootPane.add(label3, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        boxAudio = new JComboBox();
        Font boxAudioFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, -1, boxAudio.getFont());
        if (boxAudioFont != null) boxAudio.setFont(boxAudioFont);
        boxAudio.setName("audio");
        rootPane.add(boxAudio, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        boxVideo = new JComboBox();
        Font boxVideoFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, -1, boxVideo.getFont());
        if (boxVideoFont != null) boxVideo.setFont(boxVideoFont);
        boxVideo.setName("video");
        rootPane.add(boxVideo, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        boxMobile = new JComboBox();
        Font boxMobileFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, -1, boxMobile.getFont());
        if (boxMobileFont != null) boxMobile.setFont(boxMobileFont);
        boxMobile.setName("mobile");
        rootPane.add(boxMobile, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        radioAudio = new JRadioButton();
        Font radioAudioFont = this.$$$getFont$$$(null, Font.BOLD, 20, radioAudio.getFont());
        if (radioAudioFont != null) radioAudio.setFont(radioAudioFont);
        radioAudio.setName("audio");
        radioAudio.setSelected(false);
        radioAudio.setText("");
        radioAudio.setToolTipText("Only the sound will be downloaded.");
        rootPane.add(radioAudio, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        radioVideo = new JRadioButton();
        Font radioVideoFont = this.$$$getFont$$$(null, Font.BOLD, 20, radioVideo.getFont());
        if (radioVideoFont != null) radioVideo.setFont(radioVideoFont);
        radioVideo.setName("video");
        radioVideo.setText("");
        radioVideo.setToolTipText("Video will be downloaded including audio.");
        rootPane.add(radioVideo, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        radioMobile = new JRadioButton();
        Font radioMobileFont = this.$$$getFont$$$(null, Font.BOLD, 20, radioMobile.getFont());
        if (radioMobileFont != null) radioMobile.setFont(radioMobileFont);
        radioMobile.setName("mobile");
        radioMobile.setText("");
        radioMobile.setToolTipText("Video for mobile devices will be downloaded. It also contains sound.");
        rootPane.add(radioMobile, new GridConstraints(3, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        rootPane.add(spacer1, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setHorizontalAlignment(10);
        label4.setHorizontalTextPosition(10);
        label4.setText("Image:");
        label4.setVerticalAlignment(0);
        label4.setVerticalTextPosition(0);
        rootPane.add(label4, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_NORTHEAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lbThumbnail = new JLabel();
        lbThumbnail.setText("");
        rootPane.add(lbThumbnail, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setHorizontalAlignment(10);
        label5.setHorizontalTextPosition(10);
        label5.setText("Name:");
        label5.setVerticalAlignment(0);
        label5.setVerticalTextPosition(0);
        rootPane.add(label5, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        Font label6Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label6.getFont());
        if (label6Font != null) label6.setFont(label6Font);
        label6.setHorizontalAlignment(10);
        label6.setHorizontalTextPosition(10);
        label6.setText("Like / Dislike:");
        label6.setVerticalAlignment(0);
        label6.setVerticalTextPosition(0);
        rootPane.add(label6, new GridConstraints(8, 0, 1, 1, GridConstraints.ANCHOR_NORTHEAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        Font label7Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label7.getFont());
        if (label7Font != null) label7.setFont(label7Font);
        label7.setHorizontalAlignment(10);
        label7.setHorizontalTextPosition(10);
        label7.setText("Duration:");
        label7.setVerticalAlignment(0);
        label7.setVerticalTextPosition(0);
        rootPane.add(label7, new GridConstraints(9, 0, 1, 1, GridConstraints.ANCHOR_NORTHEAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lbDuration = new JLabel();
        Font lbDurationFont = this.$$$getFont$$$("DejaVu Sans", -1, -1, lbDuration.getFont());
        if (lbDurationFont != null) lbDuration.setFont(lbDurationFont);
        lbDuration.setText("");
        rootPane.add(lbDuration, new GridConstraints(9, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lbLikeDislike = new JLabel();
        Font lbLikeDislikeFont = this.$$$getFont$$$("DejaVu Sans", -1, -1, lbLikeDislike.getFont());
        if (lbLikeDislikeFont != null) lbLikeDislike.setFont(lbLikeDislikeFont);
        lbLikeDislike.setText("");
        rootPane.add(lbLikeDislike, new GridConstraints(8, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        Font label8Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label8.getFont());
        if (label8Font != null) label8.setFont(label8Font);
        label8.setHorizontalAlignment(10);
        label8.setHorizontalTextPosition(10);
        label8.setText("URL:");
        label8.setVerticalAlignment(0);
        label8.setVerticalTextPosition(0);
        rootPane.add(label8, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        rootPane.add(panel1, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        btnUrl = new JButton();
        btnUrl.setBackground(new Color(-5733));
        Font btnUrlFont = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 11, btnUrl.getFont());
        if (btnUrlFont != null) btnUrl.setFont(btnUrlFont);
        btnUrl.setText("Open");
        btnUrl.setToolTipText("Opens the default web browser with video.");
        panel1.add(btnUrl, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(30, 20), null, 0, false));
        lbUrl = new JTextArea();
        lbUrl.setEditable(false);
        Font lbUrlFont = this.$$$getFont$$$("DejaVu Sans Mono", -1, -1, lbUrl.getFont());
        if (lbUrlFont != null) lbUrl.setFont(lbUrlFont);
        lbUrl.setLineWrap(true);
        lbUrl.setText("");
        lbUrl.setWrapStyleWord(true);
        panel1.add(lbUrl, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 15), new Dimension(-1, 60), 0, false));
        final JLabel label9 = new JLabel();
        Font label9Font = this.$$$getFont$$$("DejaVu Sans", -1, -1, label9.getFont());
        if (label9Font != null) label9.setFont(label9Font);
        label9.setHorizontalAlignment(10);
        label9.setHorizontalTextPosition(10);
        label9.setText("Output file:");
        label9.setVerticalAlignment(0);
        label9.setVerticalTextPosition(0);
        rootPane.add(label9, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lbOutputFile = new JTextArea();
        lbOutputFile.setEditable(false);
        Font lbOutputFileFont = this.$$$getFont$$$(null, -1, -1, lbOutputFile.getFont());
        if (lbOutputFileFont != null) lbOutputFile.setFont(lbOutputFileFont);
        lbOutputFile.setLineWrap(true);
        lbOutputFile.setText("");
        lbOutputFile.setWrapStyleWord(true);
        rootPane.add(lbOutputFile, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(-1, 40), new Dimension(-1, 200), 0, false));
        lbName = new JTextArea();
        lbName.setEditable(false);
        Font lbNameFont = this.$$$getFont$$$(null, -1, -1, lbName.getFont());
        if (lbNameFont != null) lbName.setFont(lbNameFont);
        lbName.setLineWrap(true);
        lbName.setText("");
        lbName.setWrapStyleWord(true);
        rootPane.add(lbName, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 25), new Dimension(-1, 100), 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(2, 2, new Insets(20, 0, 0, 0), -1, -1));
        rootPane.add(panel2, new GridConstraints(10, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        btnDownload = new JButton();
        btnDownload.setBackground(new Color(-6225992));
        Font btnDownloadFont = this.$$$getFont$$$("DejaVu Sans", Font.BOLD, 14, btnDownload.getFont());
        if (btnDownloadFont != null) btnDownload.setFont(btnDownloadFont);
        btnDownload.setText("DOWNLOAD");
        btnDownload.setToolTipText("Download the video in the desired format.");
        panel2.add(btnDownload, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, 30), null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel2.add(spacer2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        panel2.add(spacer3, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        Font font = new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
        boolean isMac = System.getProperty("os.name", "").toLowerCase(Locale.ENGLISH).startsWith("mac");
        Font fontWithFallback = isMac ? new Font(font.getFamily(), font.getStyle(), font.getSize()) : new StyleContext().getFont(font.getFamily(), font.getStyle(), font.getSize());
        return fontWithFallback instanceof FontUIResource ? fontWithFallback : new FontUIResource(fontWithFallback);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPane;
    }

}
