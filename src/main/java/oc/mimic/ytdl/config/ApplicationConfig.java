package oc.mimic.ytdl.config;

import java.util.StringJoiner;

/**
 * Hlavní konfigurace aplikace.
 *
 * @author mimic
 */
public final class ApplicationConfig {

  private boolean soundsOn;
  private boolean addMetadata;
  private boolean useHttp;
  private boolean downloadThumbnail;
  private boolean history;
  private int threads;
  private String maxBatchVideoQuality;

  public boolean isSoundsOn() {
    return soundsOn;
  }

  public void setSoundsOn(boolean soundsOn) {
    this.soundsOn = soundsOn;
  }

  public boolean isAddMetadata() {
    return addMetadata;
  }

  public void setAddMetadata(boolean addMetadata) {
    this.addMetadata = addMetadata;
  }

  public boolean isUseHttp() {
    return useHttp;
  }

  public void setUseHttp(boolean useHttp) {
    this.useHttp = useHttp;
  }

  public boolean isDownloadThumbnail() {
    return downloadThumbnail;
  }

  public void setDownloadThumbnail(boolean downloadThumbnail) {
    this.downloadThumbnail = downloadThumbnail;
  }

  public boolean isHistory() {
    return history;
  }

  public void setHistory(boolean history) {
    this.history = history;
  }

  public int getThreads() {
    return threads;
  }

  public void setThreads(int threads) {
    this.threads = threads;
  }

  public String getMaxBatchVideoQuality() {
    return maxBatchVideoQuality;
  }

  public void setMaxBatchVideoQuality(String maxBatchVideoQuality) {
    this.maxBatchVideoQuality = maxBatchVideoQuality;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", ApplicationConfig.class.getSimpleName() + "[", "]").add("soundsOn=" + soundsOn)
            .add("addMetadata=" + addMetadata).add("useHttp=" + useHttp).add("downloadThumbnail=" + downloadThumbnail)
            .add("history=" + history).add("threads=" + threads)
            .add("maxBatchVideoQuality='" + maxBatchVideoQuality + "'").toString();
  }
}
