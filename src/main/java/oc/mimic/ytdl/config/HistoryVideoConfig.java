package oc.mimic.ytdl.config;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Informace o staženém videu.
 *
 * @author mimic
 */
public final class HistoryVideoConfig {

  @SerializedName("title")
  private String title;

  @SerializedName("url")
  private String videoUrl;

  @SerializedName("image")
  private String image;

  @SerializedName("output")
  private String output;

  @SerializedName("duration")
  private long duration;

  @SerializedName("shortName")
  private String shortName;

  @SerializedName("format")
  private String formatType;

  @SerializedName("done")
  private boolean done;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public boolean isDone() {
    return done;
  }

  public void setDone(boolean done) {
    this.done = done;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getOutput() {
    return output;
  }

  public void setOutput(String output) {
    this.output = output;
  }

  public long getDuration() {
    return duration;
  }

  public void setDuration(long duration) {
    this.duration = duration;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public String getFormatType() {
    return formatType;
  }

  public void setFormatType(String formatType) {
    this.formatType = formatType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    var that = (HistoryVideoConfig) o;
    return Objects.equals(videoUrl, that.videoUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(videoUrl);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", HistoryVideoConfig.class.getSimpleName() + "[", "]").add("title='" + title + "'")
            .add("videoUrl='" + videoUrl + "'").add("image='" + image + "'").add("output='" + output + "'")
            .add("duration=" + duration).add("shortName='" + shortName + "'").add("formatType='" + formatType + "'")
            .add("done=" + done).toString();
  }
}
