package oc.mimic.ytdl.config.logic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import oc.mimic.ytdl.ResourcesProvider;
import oc.mimic.ytdl.config.HistoryConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

/**
 * Správa historie stažených videi.
 *
 * @author mimic
 */
public final class HistoryProcessor {

  private static final Logger LOG = LoggerFactory.getLogger(HistoryProcessor.class);
  private static final Gson GSON = new GsonBuilder().serializeNulls().setLenient().enableComplexMapKeySerialization()
          .disableHtmlEscaping().setPrettyPrinting().create();

  /**
   * Načte historii, která se má zobrazit a zároveň se ukládat nová.
   *
   * @return Již existující nebo nová historie.
   */
  public HistoryConfig loadHistory() {
    // soubor již existuje
    var file = new File(ResourcesProvider.RES_HISTORY);
    if (file.exists() && file.isFile()) {
      LOG.info("Loading history file.");
      try {
        var json = Files.readString(file.toPath());
        return GSON.fromJson(json, HistoryConfig.class);
      } catch (IOException e) {
        LOG.error("There was an error fetching history.", e);
      }
    }
    // soubor neexistuje a bude vytvořen z resources
    var res = ResourcesProvider.getInstance();
    try (var stream = res.load(ResourcesProvider.RES_HISTORY)) {
      try (var reader = new InputStreamReader(stream, StandardCharsets.UTF_8)) {
        LOG.info("Creating history file");
        return GSON.fromJson(reader, HistoryConfig.class);
      }
    } catch (IOException e) {
      throw new RuntimeException("There was an error creating the history file.", e);
    }
  }

  /**
   * Uloží historii do souboru.
   *
   * @param history Aktuální historie.
   */
  public void save(HistoryConfig history) {
    var json = GSON.toJson(history);
    var file = new File(ResourcesProvider.RES_HISTORY);

    try {
      LOG.info("Saving history file");

      if (file.exists()) {
        file.delete();
      }
      var historyPath = Files.createFile(file.toPath());
      Files.writeString(historyPath, json, StandardOpenOption.WRITE);

    } catch (IOException e) {
      LOG.error("There was an error saving your history.", e);
    }
  }
}
