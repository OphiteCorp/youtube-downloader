package oc.mimic.ytdl.config.logic;

import oc.mimic.ytdl.config.ApplicationConfig;
import oc.mimic.ytdl.logic.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.introspector.PropertyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Spravuje konfigurační soubor pro YAML.
 *
 * @author mimic
 */
public final class YamlProcessor {

  private static final Logger LOG = LoggerFactory.getLogger(YamlProcessor.class);

  /**
   * Načte konfiguraci z YAML souboru.
   *
   * @param configFile Cesta ke konfiguračnímu souboru na classPath.
   * @param baseClass  Základní třída, ze které se má získat ClassLoader.
   *
   * @return Nová instance konfiguračního souboru.
   */
  public ApplicationConfig readConfig(String configFile, Class<?> baseClass) {
    var yaml = createYaml(ApplicationConfig.class);
    var file = new File(configFile);
    ApplicationConfig config = null;

    if (file.exists()) {
      try (var in = Files.newInputStream(file.toPath())) {
        config = yaml.loadAs(in, ApplicationConfig.class);
      } catch (Exception e) {
        LOG.warn("Error loading configuration from file. The default configuration will be used.", e);
      }
    }
    if (config == null) {
      try (var in = baseClass.getClassLoader().getResourceAsStream(configFile)) {
        config = yaml.loadAs(in, ApplicationConfig.class);

        if (config != null) {
          extractConfig(configFile, baseClass);
        } else {
          throw new RuntimeException("There was a problem extracting the configuration file.");
        }
      } catch (Exception e) {
        LOG.warn("Error loading configuration from resources.", e);
      }
    }
    if (config != null) {
      LOG.info("Application configuration loaded");
      LOG.debug("Config: {}", config);
      return config;
    }
    System.exit(0);
    return null;
  }

  private static void extractConfig(String configFile, Class<?> baseClass) {
    var file = new File(configFile);
    if (file.exists()) {
      file.delete();
    }
    try (var fos = new FileOutputStream(file)) {
      try (var resource = baseClass.getClassLoader().getResourceAsStream(configFile)) {
        var length = resource.transferTo(fos);
        LOG.debug("Config '{}' was extracted with size: {}", configFile, Utils.humanReadableFileSize(length, true));
      }
    } catch (IOException e) {
      throw new RuntimeException("An unexpected error occurred while extracting the config: " + configFile, e);
    }
  }

  private static Yaml createYaml(Class<?> configClass) {
    var c = new Constructor(configClass);
    c.setPropertyUtils(new PropertyUtils() {
      @Override
      public Property getProperty(Class<?> type, String name) {
        // umožní používat pomlčky v klíčích YAML
        if (name.indexOf('-') > -1) {
          name = Utils.toCamelCase(name, "-", false);
        }
        return super.getProperty(type, name);
      }
    });
    return new Yaml(c);
  }
}
