package oc.mimic.ytdl.config;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.StringJoiner;

/**
 * Konfigurace / nastavení historie stahování.
 *
 * @author mimic
 */
public final class HistoryConfig {

  @SerializedName("videos")
  private List<HistoryVideoConfig> videos;

  /**
   * Pokud video neexistuje, tak ho přidá.
   *
   * @param video Vstupní video.
   */
  public void addVideo(HistoryVideoConfig video) {
    if (!videos.contains(video)) {
      videos.add(video);
    }
  }

  /**
   * Odebere video podle URL.
   *
   * @param videoUrl
   */
  public void removeVideo(String videoUrl) {
    videos.removeIf(p -> p.getVideoUrl().equalsIgnoreCase(videoUrl));
  }

  /**
   * Nastaví video jako stažené.
   *
   * @param videoUrl Vstupní video.
   */
  public void setDone(String videoUrl) {
    for (var video : videos) {
      if (video.getVideoUrl().equalsIgnoreCase(videoUrl)) {
        video.setDone(true);
        break;
      }
    }
  }

  public List<HistoryVideoConfig> getVideos() {
    return videos;
  }

  public void setVideos(List<HistoryVideoConfig> videos) {
    this.videos = videos;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", HistoryConfig.class.getSimpleName() + "[", "]").add("videos=" + videos).toString();
  }
}
