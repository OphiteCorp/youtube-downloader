package oc.mimic.ytdl;

import oc.mimic.ytdl.logic.Utils;
import oc.mimic.ytdl.logic.VersionChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Správa zdrojů.
 *
 * @author mimic
 */
public final class ResourcesProvider {

  private static final Logger LOG = LoggerFactory.getLogger(ResourcesProvider.class);

  public static final String SUBDIR_WORK = "work"; // musí být bez lomítek

  private static final String SUBDIR_BIN = "bin/";
  private static final String SUBDIR_FONT = "font/";
  private static final String SUBDIR_SOUND = "sound/";
  private static final String SUBDIR_IMG = "img/";

  public static final String RES_CONFIG = "config.yml";
  public static final String RES_HISTORY = "history.json";
  public static final String RES_VERSION = "version.dat";
  public static final String RES_YOUTUBE_DL = SUBDIR_BIN + "youtube-dl.exe";
  public static final String RES_FFMPEG = SUBDIR_BIN + "ffmpeg.exe";
  public static final String RES_FFPROBE = SUBDIR_BIN + "ffprobe.exe";
  public static final String RES_FONT = SUBDIR_FONT + "DejaVuSans.ttf";
  public static final String RES_UNI_FONT = SUBDIR_FONT + "unifont-11.0.03.ttf";
  public static final String RES_CODE_FONT = SUBDIR_FONT + "DejaVuSansMono.ttf";
  public static final String RES_DOWNLOADED_SOUND = SUBDIR_SOUND + "downloaded.wav";
  public static final String RES_FAILED_SOUND = SUBDIR_SOUND + "failed.wav";
  public static final String RES_STOPPED_SOUND = SUBDIR_SOUND + "stopped.wav";
  public static final String RES_ESTABLISHED_SOUND = SUBDIR_SOUND + "established.wav";
  public static final String RES_ICON = SUBDIR_IMG + "icon.png";
  public static final String RES_DONATE = SUBDIR_IMG + "btn_donate_SM.png";

  private static final String TEMP_DIR_PREFIX;

  private static ResourcesProvider instance;

  private final Map<String, String> resourcesMap = new HashMap<>();
  private final Map<String, Font> fonts = new HashMap<>();
  private Path tempDir;

  static {
    TEMP_DIR_PREFIX = createTempDirectoryPrefix();
    LOG.debug("Temp prefix has been generated with value: '{}'", TEMP_DIR_PREFIX);
  }

  private ResourcesProvider() {
    LOG.debug("Creating temp directory");
    tempDir = Utils.createTempDirectory(TEMP_DIR_PREFIX, true);
  }

  /**
   * Získá instanci správce zsrojů.
   *
   * @return
   */
  public static synchronized ResourcesProvider getInstance() {
    if (instance == null) {
      instance = new ResourcesProvider();
    }
    return instance;
  }

  /**
   * Získá cestu k souboru podle zdroje.
   *
   * @param resource
   *
   * @return
   */
  public String getResource(String resource) {
    return resourcesMap.get(resource);
  }

  /**
   * Získá temp adresář aplikace.
   *
   * @return
   */
  public Path getTempDir() {
    return tempDir;
  }

  /**
   * Extrahuje některé zdroje do tempu.
   *
   * @return
   */
  synchronized void extractResources() {
    // zdroje, které je nutné extrahovat; verze MUSÍ být první!
    var resources = Arrays.asList(RES_VERSION, RES_YOUTUBE_DL, RES_FFMPEG, RES_FFPROBE);
    LOG.debug("Temp directory was created: {}", tempDir);
    LOG.debug("Starting resources ({}) extraction: {}", resources.size(), resources);
    var map = new HashMap<String, String>(resources.size());
    var updateAll = false; // bude True pouze v případě, že aplikace obsahuje novější knihovny

    for (var res : resources) {
      LOG.debug("Extract resource: {}", res);
      var targetAbsolutePath = tempDir + "\\" + res;
      var newFile = new File(targetAbsolutePath);
      var parent = newFile.getParentFile();

      if (!parent.exists() && !parent.mkdirs()) {
        throw new RuntimeException("An error occurred while extracting resources. Resource: " + res);
      }
      if (newFile.exists()) {
        LOG.debug("File '{}' already exists", targetAbsolutePath);

        // ověříme, jestli je aplikace novější, pokud ano, tak všechny zdroje (knihovny) aktualizujeme/přepíšeme
        if (res.equals(RES_VERSION)) {
          var version = getVersion(targetAbsolutePath);
          updateAll = version.higher;

          if (updateAll) {
            LOG.info("The application contains newer libraries. Version: {} > {}", version.appVersion,
                    version.currentVersion);
          }
        }
        if (updateAll) {
          try {
            if (Files.deleteIfExists(Paths.get(targetAbsolutePath))) {
              LOG.debug("Old file deleted: {}", targetAbsolutePath);
            }
          } catch (IOException e) {
            LOG.error("There was an error deleting the old file: " + targetAbsolutePath, e);
          }
        } else {
          map.put(res, targetAbsolutePath);
          continue;
        }
      } else {
        // pouze pro zpětnou kompatibilitu; pokud soubor s verzí neexistuje, tak je nutné všechny soubory
        // aktualizovat
        if (res.equals(RES_VERSION)) {
          updateAll = true;
        }
      }
      var resStream = Application.class.getClassLoader().getResourceAsStream(res);
      LOG.debug("Copying '{}' to '{}'", res, targetAbsolutePath);

      try (var fos = new FileOutputStream(newFile)) {
        var length = resStream.transferTo(fos);
        map.put(res, targetAbsolutePath);
        LOG.debug("File '{}' was extracted with size: {}", res, Utils.humanReadableFileSize(length, true));

      } catch (IOException e) {
        throw new RuntimeException("An unexpected error occurred while extracting the file: " + res, e);
      }
    }
    resourcesMap.putAll(map);
  }

  /**
   * Smaže pracovní temp adresář se všema zdrojema.
   */
  void deleteWorkTemp() {
    if (tempDir != null) {
      // odstraní pouze pracovní adresář
      var path = Paths.get(tempDir.toString(), SUBDIR_WORK).toString();
      LOG.debug("Deleting working temp of application files from: {}", path);
      Utils.deleteDirectory(path);
    }
  }

  /**
   * Smaže celý temp vytvořený aplikací.
   */
  void purgeTemp() {
    if (tempDir != null) {
      LOG.debug("Deleting all temp of application files from: {}", tempDir);
      Utils.deleteDirectory(tempDir.toString());
    }
  }

  /**
   * Získá font z resources.
   *
   * @param resource
   *
   * @return
   */
  public synchronized Font getFont(String resource) {
    Font font = fonts.get(resource);

    if (font == null) {
      var stream = load(resource);
      try {
        font = Font.createFont(Font.TRUETYPE_FONT, stream);
        fonts.put(resource, font);

        // zaregistruje font
        var ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(font);

      } catch (Exception e) {
        LOG.error("There was an error retrieving font from resources: " + resource, e);
      }
    }
    return font;
  }

  /**
   * Načte obrázek z resources.
   *
   * @param resource
   *
   * @return
   */
  public synchronized Image getImage(String resource) {
    var stream = load(resource);
    Image image = null;
    try {
      image = ImageIO.read(stream);
    } catch (Exception e) {
      LOG.error("There was an error loading image from resource: " + resource, e);
    }
    return image;
  }

  /**
   * Získá stream zdroje.
   *
   * @param resource
   *
   * @return
   */
  public InputStream load(String resource) {
    var stream = Application.class.getClassLoader().getResourceAsStream(resource);

    if (stream == null) {
      throw new NullPointerException("Stream for resource '" + resource + "' is null");
    }
    return stream;
  }

  /**
   * Získá verzi aplikace.
   *
   * @return Verze aplikace. Nikdy nevrací null.
   */
  public String readAppVersion() {
    try {
      try (var fis = Application.class.getClassLoader().getResourceAsStream(RES_VERSION)) {
        return new String(fis.readAllBytes());
      }
    } catch (IOException e) {
      LOG.error("There was an error reading the application version", e);
    }
    throw new RuntimeException("There was an error getting the app version");
  }

  /**
   * Získá informaci o verzi.
   *
   * @param absolutePath Absolutní cesta k souboru s verzí aplikace.
   *
   * @return Vždy vrací instanci verze.
   */
  private Version getVersion(String absolutePath) {
    String appVersion = null;
    String currentVersion = null;
    var higher = true;

    try {
      appVersion = readAppVersion();
      currentVersion = Files.readString(Paths.get(absolutePath));
      LOG.debug("The application version: {}", appVersion);
      LOG.debug("The current version: {}", currentVersion);
      higher = VersionChecker.isVersionHigher(appVersion, currentVersion);

    } catch (IOException e) {
      LOG.error("There was an error reading the application version", e);
    }
    return new Version(appVersion, currentVersion, higher);
  }

  /**
   * Vytvoří prefix pro temp adresář.
   *
   * @return
   */
  private static String createTempDirectoryPrefix() {
    return "oc.ytdl";
  }

  /**
   * Aktuální verze.
   */
  private static final class Version {

    private final String appVersion;
    private final String currentVersion;
    private final boolean higher;

    private Version(String appVersion, String currentVersion, boolean higher) {
      this.appVersion = appVersion;
      this.currentVersion = currentVersion;
      this.higher = higher;
    }
  }
}
