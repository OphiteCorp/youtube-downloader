package oc.mimic.ytdl;

import oc.mimic.ytdl.config.logic.HistoryProcessor;
import oc.mimic.ytdl.config.logic.YamlProcessor;
import oc.mimic.ytdl.gui.MainForm;
import oc.mimic.ytdl.logic.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.IOException;

/**
 * Hlavní třída aplikace.
 *
 * @author mimic
 */
public final class Application {

  private static final Logger LOG = LoggerFactory.getLogger(Application.class);

  /**
   * Hlavní metoda main().
   *
   * @param args
   *
   * @throws IOException
   */
  public static void main(String... args) {
    LOG.info("Starting...");

    var historyProcessor = new HistoryProcessor();
    var config = new YamlProcessor().readConfig(ResourcesProvider.RES_CONFIG, Application.class);
    var resHelper = ResourcesProvider.getInstance();
    var history = historyProcessor.loadHistory();

    if (args != null && args.length > 0) {
      for (var arg : args) {
        var a = arg.toLowerCase();

        if (a.equals("-clean")) {
          LOG.debug("Parameter -clean accepted");
          resHelper.purgeTemp();
        }
      }
    }
    resHelper.extractResources();

    Runtime.getRuntime().addShutdownHook(new Thread(() -> historyProcessor.save(history)));

    SwingUtilities.invokeLater(() -> {
      // nastavý výchozí vzhled
      setLookAndFeel();
      // logika před otevřením GUI
      initShutdownHook();

      // otevře GUI okno
      var form = new MainForm(config, history);
      form.setVisible(true);
    });
  }

  /**
   * Vytvoří hook, který se zavolá automaticky při ukončení aplikace jakýmkoliv způsobem.
   */
  private static void initShutdownHook() {
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      final var parent = ResourcesProvider.getInstance().getTempDir().toString();
      var processes = Utils.getProcessesFrom(parent);

      for (var p : processes) {
        try {
          LOG.info("Stopping the process with PID: {}", p.getHandle().pid());
          p.getHandle().destroyForcibly();
        } catch (Exception e) {
          LOG.error("Unable to kill process with PID: " + p.getPid(), e);
        }
      }
      LOG.info("Removing loaded resources and terminating...");
      ResourcesProvider.getInstance().deleteWorkTemp();
    }));
  }

  /**
   * Nastaví vzhled aplikace.
   */
  private static void setLookAndFeel() {
    try {
      final var requiredFeel = "Nimbus";

      for (var feel : UIManager.getInstalledLookAndFeels()) {
        if (requiredFeel.equals(feel.getName())) {
          UIManager.setLookAndFeel(feel.getClassName());
          LOG.info("Appearance set to: {}", requiredFeel);
          break;
        }
      }
    } catch (Exception e) {
      LOG.error("An error occurred while setting the appearance. Default will be used");
      UIManager.getSystemLookAndFeelClassName();
    }
  }
}
