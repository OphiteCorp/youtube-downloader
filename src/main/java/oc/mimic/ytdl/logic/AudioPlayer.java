package oc.mimic.ytdl.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Přehrávač audia.
 *
 * @author mimic
 */
public final class AudioPlayer {

  private static final Logger LOG = LoggerFactory.getLogger(AudioPlayer.class);

  /**
   * Spustí přehrávání.
   *
   * @param stream
   * @param beginTimeout
   */
  public static void play(InputStream stream, long beginTimeout) {
    var t = new Thread(new AudioProcess(stream, beginTimeout));
    t.setDaemon(true);
    t.setPriority(Thread.MIN_PRIORITY);
    t.start();
  }

  /**
   * Spustí přehrávání.
   *
   * @param stream
   */
  public static void play(InputStream stream) {
    play(stream, 0);
  }

  private static final class AudioProcess implements Runnable {

    private final InputStream stream;
    private final long beginTimeout;

    /**
     * @param stream
     * @param beginTimeout
     */
    private AudioProcess(InputStream stream, long beginTimeout) {
      this.stream = stream;
      this.beginTimeout = beginTimeout;
    }

    @Override
    public void run() {
      try {
        try (var audioIn = AudioSystem.getAudioInputStream(new BufferedInputStream(stream))) {
          var format = audioIn.getFormat();
          var info = new DataLine.Info(Clip.class, format);

          try (var clip = (Clip) AudioSystem.getLine(info)) {
            if (beginTimeout > 0) {
              Thread.sleep(beginTimeout);
            }
            clip.open(audioIn);

            var frames = (double) audioIn.getFrameLength();
            var secLength = Math.round(frames / format.getFrameRate());
            var gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(-15f);

            clip.start();
            clip.drain();

            Thread.sleep((secLength * 1000) + 500);
          }
        }
      } catch (Exception e) {
        LOG.error("There was an error while playing audio", e);
      } finally {
        try {
          stream.close();
        } catch (IOException e) {
          LOG.error("There was an unexpected error when closing the audio stream", e);
        }
      }
    }
  }
}
