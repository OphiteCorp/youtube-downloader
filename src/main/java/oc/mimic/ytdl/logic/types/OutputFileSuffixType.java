package oc.mimic.ytdl.logic.types;

/**
 * Typ přípony v názvu souboru. Jedná se stále o název, nikoli o příponu souboru.
 *
 * @author mimic
 */
public enum OutputFileSuffixType {

  BEST("best"),
  P2160("2160p"),
  P1440("1440p"),
  P1080("1080p"),
  P720("720p");

  private final String suffix;

  OutputFileSuffixType(String suffix) {
    this.suffix = suffix;
  }

  public String getSuffix() {
    return suffix;
  }
}
