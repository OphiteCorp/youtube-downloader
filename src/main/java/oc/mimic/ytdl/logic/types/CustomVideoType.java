package oc.mimic.ytdl.logic.types;

/**
 * Vlastní video formáty, které nejsou dostupné na youtube. Všechny hodnoty začínají od -100.
 *
 * @author mimic
 */
public enum CustomVideoType {

  AVI(-100, "avi", "avi");

  private final int id;
  private final String extension;
  private final String label;

  /**
   * @param id        Musí být záporné číslo, aby se snáze vyhodnotil typ.
   * @param extension Přípona typu.
   * @param label     Název typu zobrazený na formuláři.
   */
  CustomVideoType(int id, String extension, String label) {
    this.id = id;
    this.extension = extension;
    this.label = label;
  }

  /**
   * Získá typ podle ID.
   *
   * @param id
   *
   * @return
   */
  public static CustomVideoType getById(int id) {
    for (var type : values()) {
      if (type.id == id) {
        return type;
      }
    }
    return null;
  }

  /**
   * Získá počet znaků nejdelšího názvu typu.
   *
   * @return
   */
  public static int getLongestLabel() {
    var length = 0;

    for (var type : values()) {
      if (type.getLabel().length() > length) {
        length = type.getLabel().length();
      }
    }
    return length;
  }

  public String getExtension() {
    return extension;
  }

  public String getLabel() {
    return label;
  }

  public int getId() {
    return id;
  }
}
