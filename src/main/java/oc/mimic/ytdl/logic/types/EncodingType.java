package oc.mimic.ytdl.logic.types;

/**
 * Typ enkódování pro youtube-dl.
 *
 * @author mimic
 */
public enum EncodingType {

  DEFAULT(null, "Default"), // vlastní enkódování nebude použito
  UTF_8("utf8", "UTF 8");

  private final String encoding;
  private final String label;

  /**
   * @param encoding Hodnota enkódování.
   * @param label    Název zobrazený ve formuláři.
   */
  EncodingType(String encoding, String label) {
    this.encoding = encoding;
    this.label = label;
  }

  public String getEncoding() {
    return encoding;
  }

  public String getLabel() {
    return label;
  }
}
