package oc.mimic.ytdl.logic.types;

/**
 * Typ vybraného formátu pro lepší správu.
 *
 * @author mimic
 */
public enum FormatType {

  AUDIO("audio"),
  VIDEO("video"),
  MOBILE("mobile");

  private final String label;

  FormatType(String label) {
    this.label = label;
  }

  public String getLabel() {
    return label;
  }
}
