package oc.mimic.ytdl.logic.types;

/**
 * Vlastní audio formáty, které nejsou dostupné na youtube. Všechny hodnoty začínají od -1.
 *
 * @author mimic
 */
public enum CustomAudioType {

  MP3(-1, "mp3", "mp3", "mp3"),
  WAV(-2, "wav", "wav", "wav"),
  FLAC(-3, "flac", "flac", "flac"),
  OGG(-4, "vorbis", "ogg", "ogg"),
  OPUS(-5, "opus", "opus", "opus");

  private final int id;
  private final String value;
  private final String newValue;
  private final String label;

  /**
   * @param id       Musí být záporné číslo, aby se snáze vyhodnotil typ.
   * @param value    Hodnota vrácená z youtube.
   * @param newValue Nová naše hodnota.
   * @param label    Název typu zobrazený na formuláři.
   */
  CustomAudioType(int id, String value, String newValue, String label) {
    this.id = id;
    this.value = value;
    this.newValue = newValue;
    this.label = label;
  }

  /**
   * Získá typ podle ID.
   *
   * @param id
   *
   * @return
   */
  public static CustomAudioType getById(int id) {
    for (var type : values()) {
      if (type.id == id) {
        return type;
      }
    }
    return null;
  }

  /**
   * Získá počet znaků nejdelšího názvu typu.
   *
   * @return
   */
  public static int getLongestLabel() {
    var length = 0;

    for (var type : values()) {
      if (type.getLabel().length() > length) {
        length = type.getLabel().length();
      }
    }
    return length;
  }

  public String getValue() {
    return value;
  }

  public String getLabel() {
    return label;
  }

  public int getId() {
    return id;
  }

  public String getNewValue() {
    return newValue;
  }
}
