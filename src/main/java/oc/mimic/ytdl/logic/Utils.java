package oc.mimic.ytdl.logic;

import oc.mimic.ytdl.logic.dto.ProcessData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oshi.software.os.windows.WindowsOperatingSystem;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Pomocné metody.
 *
 * @author mimic
 */
public final class Utils {

  private static final Logger LOG = LoggerFactory.getLogger(Utils.class);

  private static final WindowsOperatingSystem OS = new WindowsOperatingSystem();

  /**
   * Získá naformátovanou velikost souboru.
   *
   * @param fileSize Velikost souboru v bytech.
   * @param binary   True, pokud má být 1024 byte formát.
   *
   * @return
   */
  public static String humanReadableFileSize(long fileSize, boolean binary) {
    var unit = binary ? 1024 : 1000;
    if (fileSize < unit) {
      return fileSize + " B";
    }
    var exp = (int) (Math.log(fileSize) / Math.log(unit));
    var pre = (binary ? "KMGTPE" : "kMGTPE").charAt(exp - 1) + (binary ? "i" : "");
    return String.format("%.2f %sB", fileSize / Math.pow(unit, exp), pre);
  }

  /**
   * Odsadí vstupní text z levé strany. Pro odsazení použije mezery.
   *
   * @param str    Vstupní text.
   * @param length Odsazení včetně délky textu.
   *
   * @return
   */
  public static String lPad(String str, int length) {
    return lPad(str, length, ' ');
  }

  /**
   * Odsadí vstupní text z levé strany.
   *
   * @param str    Vstupní text.
   * @param length Odsazení včetně délky textu.
   * @param ch     Znak použitý pro osazení.
   *
   * @return
   */
  public static String lPad(String str, int length, char ch) {
    return (str + String.format("%" + length + "s", "").replace(" ", String.valueOf(ch))).substring(0, length);
  }

  /**
   * Odsadí vstupní text z pracé strany.
   *
   * @param str    Vstupní text.
   * @param length Odsazení včetně délky textu.
   * @param ch     Znak použitý pro osazení.
   *
   * @return
   */
  public static String rPad(String str, int length, char ch) {
    return (String.format("%" + length + "s", "").replace(" ", String.valueOf(ch)) + str)
            .substring(str.length(), length + str.length());
  }

  /**
   * Odsadí vstupní text z pravé strany. Pro odsazení použije mezery.
   *
   * @param str    Vstupní text.
   * @param length Odsazení včetně délky textu.
   *
   * @return
   */
  public static String rPad(String str, int length) {
    return rPad(str, length, ' ');
  }

  /**
   * Načte obrázek z URL.
   *
   * @param imageUrl URL obrazku.
   *
   * @return
   */
  public static Image readImage(String imageUrl) {
    Image image = null;
    try {
      var url = new URL(imageUrl);
      image = ImageIO.read(url);
    } catch (IOException e) {
      LOG.error("There was an error loading image from URL: " + imageUrl);
    }
    return image;
  }

  /**
   * Načte obrázek z adresáře.
   *
   * @param imageFile Obrázek.
   *
   * @return
   */
  public static Image readImage(File imageFile) {
    Image image = null;
    try (var fis = new FileInputStream(imageFile)) {
      image = ImageIO.read(fis);
    } catch (IOException e) {
      LOG.error("There was an error loading image from URL: " + imageFile.getAbsolutePath());
    }
    return image;
  }

  /**
   * Převede obrázek do BufferedImage.
   *
   * @param img Obrázek.
   *
   * @return
   */
  public static BufferedImage toBufferedImage(Image img) {
    if (img instanceof BufferedImage) {
      return (BufferedImage) img;
    }
    var bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    var bGr = bimage.createGraphics();
    bGr.drawImage(img, 0, 0, null);
    bGr.dispose();
    return bimage;
  }

  /**
   * Převede obrázek do base64.
   *
   * @param image Vstupní obrázek.
   *
   * @return
   */
  public static String convertToBase64(BufferedImage image) {
    try (var baos = new ByteArrayOutputStream()) {
      ImageIO.write(image, "png", baos);
      var imageBytes = baos.toByteArray();
      return Base64.getEncoder().encodeToString(imageBytes);

    } catch (IOException e) {
      throw new RuntimeException("There was an error converting the image to base64.", e);
    }
  }

  /**
   * Převede base64 zpět na obrázek.
   *
   * @param imageBase64 Vstupní obrázek jako base64.
   *
   * @return
   */
  public static BufferedImage convertToImage(String imageBase64) {
    try {
      var stream = new ByteArrayInputStream(Base64.getDecoder().decode(imageBase64.getBytes()));
      return ImageIO.read(stream);
    } catch (IOException e) {
      throw new RuntimeException("There was an error converting base64 to an image.", e);
    }
  }

  /**
   * Vypočte novou velikost obrázku při ponechání aspect ratia.
   *
   * @param image    Vstupní obrázek.
   * @param boundary Požadovaný rozměry.
   *
   * @return
   */
  public static Dimension calculateNewRatioSize(Image image, Dimension boundary) {
    var width = image.getWidth(null);
    var height = image.getHeight(null);

    var widthRatio = boundary.getWidth() / width;
    var heightRatio = boundary.getHeight() / height;
    var ratio = Math.min(widthRatio, heightRatio);

    return new Dimension((int) (width * ratio), (int) (height * ratio));
  }

  /**
   * Získá aktuální adresář.
   *
   * @return
   */
  public static File getCurrentDirectory() {
    return new File(".");
  }

  /**
   * Získá příponu souboru.
   *
   * @param file Název souboru. Může obsahovat i cestu.
   *
   * @return
   */
  public static String getFileExtension(String file) {
    var extension = "";
    var i = file.lastIndexOf('.');
    var p = Math.max(file.lastIndexOf('/'), file.lastIndexOf('\\'));

    if (i > p) {
      extension = file.substring(i + 1);
    }
    return extension;
  }

  /**
   * Změní velikost obrázku.
   *
   * @param image     Původní obrázek.
   * @param newWidth  Nová velikost X.
   * @param newHeight Nová velikost Y.
   *
   * @return Nový obrázek.
   */
  public static Image scaleImage(Image image, int newWidth, int newHeight) {
    var newSize = calculateNewRatioSize(image, new Dimension(newWidth, newHeight));
    return image.getScaledInstance((int) newSize.getWidth(), (int) newSize.getHeight(), Image.SCALE_SMOOTH);
  }

  /**
   * Otevře prohlížeč s URL.
   *
   * @param url
   *
   * @return True, pokud vše proběhlo v pořádku.
   */
  public static boolean openBrowser(String url) {
    try {
      LOG.debug("Opening a URL in a browser: {}", url);
      Desktop.getDesktop().browse(new URI(url));
      return true;

    } catch (Exception e) {
      LOG.error("There was an error opening the URL: " + url, e);
      return false;
    }
  }

  /**
   * Vytvoří adresář v temp.
   *
   * @param prefix     Prefix adresáře.
   * @param customTemp Pokud bude true, tak se nastaví vlastní cesta k tempu.
   *
   * @return
   */
  public static Path createTempDirectory(String prefix, boolean customTemp) {
    try {
      Path tempDir;
      if (customTemp) {
        tempDir = Paths.get(System.getProperty("java.io.tmpdir"), prefix);
        var dir = tempDir.toFile();

        if (!dir.exists()) {
          dir.mkdirs();
        }
      } else {
        tempDir = Files.createTempDirectory(prefix + ".");
      }
      Files.setAttribute(tempDir, "dos:hidden", true);
      Files.setAttribute(tempDir, "dos:system", true);
      return tempDir;
    } catch (IOException e) {
      throw new RuntimeException("There was an error creating temp directory");
    }
  }

  /**
   * Vymaže adresář a všechny jeho podadresáře.
   *
   * @param dir
   */
  public static void deleteDirectory(String dir) {
    var file = new File(dir);

    if (file.exists()) {
      try {
        Files.walk(file.toPath()).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
      } catch (IOException e) {
        LOG.error("There was an error deleting the directory: " + dir, e);
      }
    }
  }

  /**
   * Překopíruje soubor do jiného umístění.
   *
   * @param source
   * @param dest
   * @param event
   *
   * @throws IOException
   */
  public static void copyFile(File source, File dest, ICopyEvent event) throws IOException {
    var size = source.length();

    try (var fis = new FileInputStream(source)) {
      try (var fos = new FileOutputStream(dest)) {
        var buffer = new byte[1024 * 8];
        var totalCopy = 0L;
        int read;

        while ((read = fis.read(buffer)) > 0) {
          fos.write(buffer, 0, read);
          totalCopy += read;

          if (event != null) {
            event.progress(read, totalCopy, size);
          }
        }
      }
    }
  }

  /**
   * Převede délku videa na čitelný formát.
   *
   * @param duration Délka videa v skundách.
   *
   * @return
   */
  public static String convertToReadableDuration(long duration) {
    var hours = duration / 3600;
    var minutes = (duration % 3600) / 60;
    var seconds = duration % 60;
    var output = "";

    if (hours > 0) {
      output += hours + " h ";
    }
    if (hours > 0 || minutes > 0) {
      output += minutes + " min ";
    }
    if (hours > 0 || minutes > 0 || seconds > 0) {
      output += seconds + " sec ";
    }
    return output;
  }

  /**
   * Získá všechny běžící procesy z adresáře.
   *
   * @param parentDirectory
   *
   * @return
   */
  public static synchronized java.util.List<ProcessData> getProcessesFrom(String parentDirectory) {
    var processes = new ArrayList<ProcessData>();
    var handles = ProcessHandle.allProcesses().collect(Collectors.toList());

    for (var handle : handles) {
      if (handle.info().command().isPresent() && handle.info().command().get().startsWith(parentDirectory)) {
        var process = OS.getProcess((int) handle.pid());
        if (process != null) {
          LOG.debug("Native process load for PID: {}", handle.pid());
          processes.add(new ProcessData(handle, process));
        }
      }
    }
    return processes;
  }

  /**
   * Převede text do camelCase.
   *
   * @param str        Vstupní text.
   * @param delimiter  Delimiter (třeba pomlčka, tečka, ..).
   * @param upperFirst Má se nastavit pvní znak upperCase?
   *
   * @return Upravený vstupní text.
   */
  public static String toCamelCase(String str, String delimiter, boolean upperFirst) {
    var parts = str.split(delimiter);
    var sb = new StringBuilder();

    for (var part : parts) {
      var value = part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase();
      sb.append(value);
    }
    var name = sb.toString();

    if (!upperFirst) {
      name = name.substring(0, 1).toLowerCase() + name.substring(1);
    }
    return name;
  }

  /**
   * Stáhne soubor z URL.
   *
   * @param inputUrl  Vstupní URL soubor.
   * @param extension Nová přípona souboru.
   *
   * @return Stažený souboru.
   */
  public static File downloadFile(String inputUrl, String extension) {
    try {
      var uuid = UUID.randomUUID().toString().replaceAll("-", "");
      var tempFile = File.createTempFile(uuid, "." + extension);
      var url = new URL(inputUrl);

      try (var streamChannel = Channels.newChannel(url.openStream())) {
        try (var fos = new FileOutputStream(tempFile)) {
          fos.getChannel().transferFrom(streamChannel, 0, Long.MAX_VALUE);
          return tempFile;
        }
      }
    } catch (Exception e) {
      LOG.debug("Error downloading file from URL: {}", inputUrl);
      return null;
    }
  }
}
