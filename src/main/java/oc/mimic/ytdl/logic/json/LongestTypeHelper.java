package oc.mimic.ytdl.logic.json;

import java.util.List;

/**
 * Logika pro výpočet nejdelší hodnoty.
 *
 * @author mimic
 */
public final class LongestTypeHelper {

  /**
   * Vypočte nejdelší hodnotu podle typu. Hodnoty bere z dostupných formátů.
   *
   * @param longestType
   * @param formats
   *
   * @return
   */
  public static int calculate(LongestType longestType, List<VideoFormatJson> formats) {
    var length = 0;

    if (formats != null) {
      var currentLength = 0;

      for (var format : formats) {
        if (format == null) {
          continue;
        }
        switch (longestType) {
          case FPS:
            currentLength = String.valueOf(format.getFps()).length();
            break;

          case FORMAT_NOTE:
            currentLength = (format.getFormatNote() != null) ? format.getFormatNote().length() : 0;
            break;

          case EXTENSION:
            currentLength = (format.getExtension() != null) ? format.getExtension().length() : 0;
            break;

          case BETTER_EXTENSION:
            currentLength = (format.getBetterExtension() != null) ? format.getBetterExtension().length() : 0;
            break;

          case VIDEO_CODEC:
            currentLength = (format.getVideoCodec() != null) ? format.getVideoCodec().length() : 0;
            break;

          case AUDIO_CODEC:
            currentLength = (format.getAudioCodec() != null) ? format.getAudioCodec().length() : 0;
            break;
        }
        if (currentLength > length) {
          length = currentLength;
        }
      }
    }
    return length;
  }
}
