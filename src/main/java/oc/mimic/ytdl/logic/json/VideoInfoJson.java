package oc.mimic.ytdl.logic.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Informace o videu z youtube.
 *
 * @author mimic
 */
public final class VideoInfoJson {

  @SerializedName("uploader_url")
  private String uploaderUrl;

  @SerializedName("average_rating")
  private double averageRating;

  @SerializedName("display_id")
  private String displayId;

  @SerializedName("ext")
  private String extension;

  @SerializedName("channel_id")
  private String channelId;

  @SerializedName("title")
  private String title;

  @SerializedName("abr")
  private double avarageBitrate;

  @SerializedName("width")
  private int width;

  @SerializedName("height")
  private int height;

  @SerializedName("channel_url")
  private String channelUrl;

  @SerializedName("duration")
  private long duration;

  @SerializedName("dislike_count")
  private long dislikeCount;

  @SerializedName("acodec")
  private String audioCodec;

  @SerializedName("webpage_url")
  private String webPageUrl;

  @SerializedName("thumbnail")
  private String thumbnailUrl;

  @SerializedName("format_id")
  private String formatId;

  @SerializedName("upload_date")
  private long uploadDate;

  @SerializedName("view_count")
  private long viewCount;

  @SerializedName("uploader_id")
  private String uploaderId;

  @SerializedName("age_limit")
  private int ageLimit;

  @SerializedName("uploader")
  private String uploader;

  @SerializedName("format")
  private String format;

  @SerializedName("like_count")
  private long likeCount;

  @SerializedName("id")
  private String id;

  @SerializedName("fps")
  private int fps;

  @SerializedName("vcodec")
  private String videoCodec;

  @SerializedName("description")
  private String description;

  @SerializedName("is_live")
  private boolean live;

  @SerializedName("categories")
  private List<String> categories;

  @SerializedName("tags")
  private List<String> tags;

  @SerializedName("formats")
  private List<VideoFormatJson> formats;

  @SerializedName("requested_formats")
  private List<VideoFormatJson> requestedFormats;

  public boolean isLive() {
    return live;
  }

  public void setLive(boolean live) {
    this.live = live;
  }

  public String getTitle() {
    return title;
  }

  public String getUploaderUrl() {
    return uploaderUrl;
  }

  public double getAverageRating() {
    return averageRating;
  }

  public String getDisplayId() {
    return displayId;
  }

  public String getChannelId() {
    return channelId;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public String getChannelUrl() {
    return channelUrl;
  }

  public long getDuration() {
    return duration;
  }

  public long getDislikeCount() {
    return dislikeCount;
  }

  public double getAvarageBitrate() {
    return avarageBitrate;
  }

  public String getAudioCodec() {
    return audioCodec;
  }

  public String getWebPageUrl() {
    return webPageUrl;
  }

  public String getVideoCodec() {
    return videoCodec;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public String getFormatId() {
    return formatId;
  }

  public long getUploadDate() {
    return uploadDate;
  }

  public long getViewCount() {
    return viewCount;
  }

  public String getUploaderId() {
    return uploaderId;
  }

  public int getAgeLimit() {
    return ageLimit;
  }

  public String getUploader() {
    return uploader;
  }

  public String getFormat() {
    return format;
  }

  public long getLikeCount() {
    return likeCount;
  }

  public String getId() {
    return id;
  }

  public int getFps() {
    return fps;
  }

  public String getExtension() {
    return extension;
  }

  public String getDescription() {
    return description;
  }

  public List<String> getCategories() {
    return categories;
  }

  public List<String> getTags() {
    return tags;
  }

  public List<VideoFormatJson> getFormats() {
    return formats;
  }

  public List<VideoFormatJson> getRequestedFormats() {
    return requestedFormats;
  }

  @Override
  public String toString() {
    return format;
  }
}
