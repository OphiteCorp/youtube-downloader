package oc.mimic.ytdl.logic.json;

/**
 * Typ nejdelší hodnoty pro výpočet.
 *
 * @author mimic
 */
public enum LongestType {

  FPS,
  FORMAT_NOTE,
  EXTENSION,
  BETTER_EXTENSION,
  VIDEO_CODEC,
  AUDIO_CODEC
}
