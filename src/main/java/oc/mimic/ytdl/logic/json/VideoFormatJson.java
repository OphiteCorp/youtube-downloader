package oc.mimic.ytdl.logic.json;

import com.google.gson.annotations.SerializedName;

/**
 * Informace o formátu youtube videa.
 *
 * @author mimic
 */
public final class VideoFormatJson {

  @SerializedName("abr")
  private double avarageBitrate;

  @SerializedName("width")
  private int width;

  @SerializedName("height")
  private int height;

  @SerializedName("vcodec")
  private String videoCodec;

  @SerializedName("container")
  private String container;

  @SerializedName("url")
  private String url;

  @SerializedName("filesize")
  private long filesize;

  @SerializedName("tbr")
  private double tbr;

  @SerializedName("format_note")
  private String formatNote;

  @SerializedName("quality")
  private int quality;

  @SerializedName("protocol")
  private String protocol;

  @SerializedName("format_id")
  private int formatId;

  @SerializedName("acodec")
  private String audioCodec;

  @SerializedName("format")
  private String format;

  @SerializedName("ext")
  private String extension;

  @SerializedName("fps")
  private int fps;

  /**
   * Získá lepší příponu souboru, kde nahrazuje některé přípony za více relevantní např. webm -> mkv.
   *
   * @return
   */
  public final String getBetterExtension() {
    if (extension != null && extension.equalsIgnoreCase("webm")) {
      return "mkv";
    }
    return getExtension();
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public String getContainer() {
    return container;
  }

  public String getUrl() {
    return url;
  }

  public long getFilesize() {
    return filesize;
  }

  public double getTbr() {
    return tbr;
  }

  public String getFormatNote() {
    return formatNote;
  }

  public int getQuality() {
    return quality;
  }

  public String getProtocol() {
    return protocol;
  }

  public int getFormatId() {
    return formatId;
  }

  public String getFormat() {
    return format;
  }

  public double getAvarageBitrate() {
    return avarageBitrate;
  }

  public String getVideoCodec() {
    return videoCodec;
  }

  public String getAudioCodec() {
    return audioCodec;
  }

  public String getExtension() {
    return extension;
  }

  public int getFps() {
    return fps;
  }

  @Override
  public String toString() {
    return format;
  }
}
