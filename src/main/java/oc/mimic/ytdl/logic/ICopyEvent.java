package oc.mimic.ytdl.logic;

/**
 * Rozhraní pro událost při kopírování souborů.
 *
 * @author mimic
 */
public interface ICopyEvent {

  /**
   * Stav kopírování.
   *
   * @param read    Přečteno bytů.
   * @param current Celkem nakopírováno.
   * @param size    Celková velikost souboru.
   */
  void progress(long read, long current, long size);
}
