package oc.mimic.ytdl.logic.dto;

import oshi.software.os.OSProcess;

/**
 * Informace o procesu.
 *
 * @author mimic
 */
public final class ProcessData {

  private final ProcessHandle handle;
  private final OSProcess process;
  private final int pid;

  /**
   * @param handle
   * @param process
   */
  public ProcessData(ProcessHandle handle, OSProcess process) {
    this.handle = handle;
    this.process = process;
    pid = process.getProcessID();
  }

  public ProcessHandle getHandle() {
    return handle;
  }

  public OSProcess getProcess() {
    return process;
  }

  public int getPid() {
    return pid;
  }
}
