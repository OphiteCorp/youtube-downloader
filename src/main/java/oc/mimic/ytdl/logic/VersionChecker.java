package oc.mimic.ytdl.logic;

import java.util.regex.Pattern;

/**
 * Pomocní třída pro kontrolu verze.
 * <br>
 * Ukázka:
 * <p>
 * isVersionHigher("3.10.0", "3.4.0");
 * isVersionHigher("5.4.2", "5.4.1");
 * isVersionHigher("5.4.4", "5.4.5");
 * isVersionHigher("5.4.9", "5.4.12");
 * isVersionHigher("5.9.222", "5.10.12");
 * isVersionHigher("5.10.12", "5.10.12");
 * isVersionHigher("5.10.13", "5.10.14");
 * isVersionHigher("6.7.0", "6.8");
 * isVersionHigher("6.7", "2.7.0");
 * isVersionHigher("6", "6.3.1");
 * isVersionHigher("4", "4.0.0");
 * isVersionHigher("6.3.0", "6");
 * isVersionHigher("5.10.12-Alpha", "5.10.12-beTA");
 * isVersionHigher("5.10.13-release", "5.10.14-beta");
 * isVersionHigher("6.7.0", "6.8-snapshot");
 * isVersionHigher("6.7.1", "6.7.0-release");
 * isVersionHigher("6-snapshot", "6.0.0-beta");
 * isVersionHigher("6.0-snapshot", "6.0.0-whatthe");
 * isVersionHigher("5.10.12-Alpha-1", "5.10.12-alpha-2");
 * isVersionHigher("5.10.13-release-1", "5.10.13-release2");
 * isVersionHigher("10-rc42", "10.0.0-rc53");
 * </p>
 *
 * @author mimic
 */
public final class VersionChecker {

  /**
   * Porovná verze a zjistí, jestli se jedná o novější verzi.
   *
   * @param baseVersion Aktuální verze aplikace.
   * @param testVersion Testovaná verze (např. z souboru na disku).
   *
   * @return True, pokud aplikace obsahuje novější verzi.
   */
  public static boolean isVersionHigher(String baseVersion, String testVersion) {
    return versionToComparable(baseVersion).compareTo(versionToComparable(testVersion)) > 0;
  }

  /**
   * Porovná verze a zjistí, jestli se jedná o starší verzi.
   *
   * @param baseVersion Aktuální verze aplikace.
   * @param testVersion Testovaná verze (např. z souboru na disku).
   *
   * @return True, pokud aplikace obsahuje starší verzi.
   */
  public static boolean isVersionLower(String baseVersion, String testVersion) {
    return versionToComparable(baseVersion).compareTo(versionToComparable(testVersion)) < 0;
  }

  /**
   * Porovná verze a zjistí, jestli jsou verze stejné.
   *
   * @param baseVersion Aktuální verze aplikace.
   * @param testVersion Testovaná verze (např. z souboru na disku).
   *
   * @return True, pokud aplikace obsahuje stejnou verzi.
   */
  public static boolean isVersionSame(String baseVersion, String testVersion) {
    return versionToComparable(baseVersion).compareTo(versionToComparable(testVersion)) == 0;
  }

  private static String versionToComparable(String version) {
    var versionNum = version;
    var at = version.indexOf('-');

    if (at >= 0) {
      versionNum = version.substring(0, at);
    }
    var numAr = versionNum.split("\\.");
    var versionFormatted = new StringBuilder("0");

    for (var tmp : numAr) {
      versionFormatted.append(String.format("%4s", tmp).replace(' ', '0'));
    }
    while (versionFormatted.length() < 12) {
      versionFormatted.append("0000");
    }
    return versionFormatted + getVersionModifier(version, at);
  }

  private static String getVersionModifier(String version, int at) {
    var wordModsAr = new String[]{ "-SNAPSHOT", "-ALPHA", "-BETA", "-RC", "-RELEASE" };

    if (at < 0) {
      return "." + wordModsAr.length + "00";
    }
    var i = 1;
    for (var word : wordModsAr) {
      if ((at = version.toUpperCase().indexOf(word)) > 0) {
        return "." + i + getSecondVersionModifier(version.substring(at + word.length()));
      }
      i++;
    }
    return ".000";
  }

  private static String getSecondVersionModifier(String version) {
    var m = Pattern.compile("(.*?)(\\d+).*").matcher(version);
    return m.matches() ? String.format("%2s", m.group(2)).replace(' ', '0') : "00";
  }
}
