package oc.mimic.ytdl.logic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import oc.mimic.ytdl.ResourcesProvider;
import oc.mimic.ytdl.logic.json.VideoInfoJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Stáhne informace o videu pomocí youtube-dl.
 *
 * @author mimic
 */
public final class DumpVideoData implements Callable<VideoInfoJson> {

  private static final Logger LOG = LoggerFactory.getLogger(DumpVideoData.class);
  private static final Gson GSON = new GsonBuilder().setLenient().create();

  private final String youtubeVideo;

  /**
   * @param youtubeVideo
   */
  public DumpVideoData(String youtubeVideo) {
    this.youtubeVideo = youtubeVideo;
  }

  /**
   * Stáhne informace o videu.
   *
   * @return
   */
  @Override
  public VideoInfoJson call() {
    LOG.info("Get video info from: {}", youtubeVideo);

    var ytDlBin = ResourcesProvider.getInstance().getResource(ResourcesProvider.RES_YOUTUBE_DL);
    var commands = prepareCommands(ytDlBin, youtubeVideo);
    var fullCommand = String.join(" ", commands);
    LOG.debug("Assembled command: {}", fullCommand);

    var builder = new ProcessBuilder().command(commands);
    builder.redirectError(ProcessBuilder.Redirect.PIPE);
    builder.redirectOutput(ProcessBuilder.Redirect.PIPE);
    VideoInfoJson videoInfo = null;

    try {
      var proc = builder.start();
      LOG.debug("The process was started...");

      try (var reader = new BufferedReader(new InputStreamReader(proc.getInputStream(), StandardCharsets.UTF_8))) {

        String line;

        while ((line = reader.readLine()) != null) {
          if (line.trim().length() > 0 && videoInfo == null) {
            videoInfo = parseToVideoData(line);

            if (videoInfo != null) {
              LOG.info("Video information about video '{}' was successfully acquired", youtubeVideo);
            }
          }
          LOG.trace(line);
        }
      }
      try (var reader = new BufferedReader(new InputStreamReader(proc.getErrorStream(), StandardCharsets.UTF_8))) {

        String line;

        while ((line = reader.readLine()) != null) {
          LOG.error(line);
        }
      }
      var status = proc.waitFor();
      if (status != 0) {
        LOG.warn("The process of retrieving video information returned a different status code: {}", status);
      }
    } catch (IOException e) {
      LOG.error("An unexpected error occurred while getting a video information", e);
    } catch (InterruptedException e) {
      LOG.error("During the process, the application was terminated unexpectedly", e);
    }
    return videoInfo;
  }

  /**
   * Připravá příkaz s parametry pro stažení informací o videu.
   *
   * @param ytDlBin
   * @param youtubeVideo
   *
   * @return
   */
  private static List<String> prepareCommands(String ytDlBin, String youtubeVideo) {
    var commands = new ArrayList<String>();
    commands.add(ytDlBin);
    commands.add("-J");
    commands.add("\"" + youtubeVideo + "\"");
    return commands;
  }

  /**
   * Převede výstup (json) z youtube-dl do objektu.
   *
   * @param outputJson
   *
   * @return
   */
  private static VideoInfoJson parseToVideoData(String outputJson) {
    try {
      return GSON.fromJson(outputJson, VideoInfoJson.class);
    } catch (Exception e) {
      LOG.error("There was an error converting the output to video information", e);
      return null;
    }
  }
}
