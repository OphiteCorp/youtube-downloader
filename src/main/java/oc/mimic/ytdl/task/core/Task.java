package oc.mimic.ytdl.task.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Základní implementace tásku.
 *
 * @author mimic
 */
public abstract class Task implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(Task.class);

  private static volatile AtomicLong sid = new AtomicLong(0);

  private final List<Task> subTasks = new ArrayList<>(0);
  private final TaskHandler handler = new TaskHandler();
  private final long id;

  protected Task() {
    id = sid.incrementAndGet();
    LOG.debug("[{}] Task was created", id);
  }

  /**
   * Logika, která se má vykonat po dokončení zpracování tásku.
   */
  protected void postProcess() {
  }

  /**
   * Logika, která se má vykonat před zahájením zpracování tásku.
   */
  protected void preProcess() {
  }

  /**
   * Samotný proces tásku.
   *
   * @return True, pokud vše proběhlo v pořádku.
   *
   * @throws Exception
   */
  protected abstract boolean process() throws Exception;

  /**
   * Přidá pod-tásk k hlavnímu tásku.
   *
   * @param task
   */
  protected final void addSubTask(Task task) {
    subTasks.add(task);
  }

  /**
   * Odebere pod-tásk z hlavního tásku.
   *
   * @param task
   */
  protected final void removeSubTask(Task task) {
    subTasks.remove(task);
  }

  /**
   * Přidá listener pro události nad táskem.
   *
   * @param listener
   */
  public final void addListener(ITaskListener listener) {
    handler.listeners.add(listener);
  }

  /**
   * Odebere listener pro události nad táskem.
   *
   * @param listener
   */
  public final void removeListener(ITaskListener listener) {
    handler.listeners.remove(listener);
  }

  /**
   * Získá unikátní ID tásku.
   *
   * @return
   */
  public final long getId() {
    return id;
  }

  @Override
  public final void run() {
    var success = true;
    handler.fireStarted(this);
    preProcess();
    try {
      success = process(); // hlavní tásk

      // zpracuje všechny pod-tásky
      for (var subTask : subTasks) {
        LOG.debug("[{}] Sub-Task with ID '{}' was submited to process", id, subTask.getId());
        TaskManager.getInstance(null).submit(subTask);
      }
    } catch (Exception e) {
      LOG.error("[" + id + "] There was an error processing the task", e);
      success = false;
    }
    postProcess();
    handler.fireFinished(this, success);
    LOG.debug("[{}] Task has been completely completed", id);
  }

  /**
   * Handler pro události nad táskem.
   */
  private static final class TaskHandler {

    private final List<ITaskListener> listeners = new CopyOnWriteArrayList<>();

    /**
     * Odešle událost o zahájení tásku všem listenerům.
     *
     * @param task
     */
    private synchronized void fireStarted(Task task) {
      LOG.debug("Fire started task ID: {}", task.getId());
      for (var listener : listeners) {
        listener.started(task);
      }
    }

    /**
     * Odešle událost o dokončení tásku všem listenerům.
     *
     * @param task
     * @param success
     */
    private synchronized void fireFinished(Task task, boolean success) {
      LOG.debug("Fire finished task ID: {}", task.getId());
      for (var listener : listeners) {
        listener.finished(task, success);
      }
    }
  }
}
