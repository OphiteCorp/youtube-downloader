package oc.mimic.ytdl.task.core;

import oc.mimic.ytdl.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;

/**
 * Správa všech tásků.
 *
 * @author mimic
 */
public final class TaskManager {

  private static final Logger LOG = LoggerFactory.getLogger(TaskManager.class);

  private static TaskManager instance;

  private final ExecutorService pool;
  private final ScheduledExecutorService scheduledPool;

  /**
   * @param executors Počet vláken. Pokud bude null nebo 0, tak se použije počet vláken dle počtu jader CPU.
   */
  private TaskManager(Integer executors) {
    var processors = Runtime.getRuntime().availableProcessors();
    LOG.debug("Number of available processors: {}", processors);

    if (executors != null && executors > 0) {
      processors = executors;
      LOG.debug("The number of processors has been set to: {}", executors);
    }
    pool = Executors.newFixedThreadPool(processors);
    scheduledPool = Executors.newScheduledThreadPool(1);
    LOG.debug("Task manager was created");
  }

  /**
   * Získá instanci správce.
   *
   * @param config Konfigurace aplikace.
   *
   * @return
   */
  public static synchronized TaskManager getInstance(ApplicationConfig config) {
    if (TaskManager.instance == null) {
      TaskManager.instance = new TaskManager(config.getThreads());
    }
    return TaskManager.instance;
  }

  /**
   * Počká na dokončení všech tásků.
   *
   * @param timeout Jak dlouho se má čekat.
   * @param unit    Typ hodnoty timeout.
   *
   * @return
   *
   * @throws InterruptedException
   */
  public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
    return pool.awaitTermination(timeout, unit);
  }

  /**
   * Získá počet tásků ve frontě.
   *
   * @return
   */
  public int getQueueSize() {
    return ((ThreadPoolExecutor) pool).getQueue().size();
  }

  /**
   * Získá počet aktivních tásků.
   *
   * @return
   */
  public int getActiveTasks() {
    return ((ThreadPoolExecutor) pool).getActiveCount();
  }

  /**
   * Začně zpracovávat tásk async.
   *
   * @param task
   *
   * @return
   */
  public TaskManager execute(Task task) {
    pool.execute(task);
    return this;
  }

  /**
   * Je správce vypnut?
   *
   * @return
   */
  public boolean isShutdown() {
    return pool.isShutdown();
  }

  /**
   * Je správce vypnut a všechny tásky ukončeny?
   *
   * @return
   */
  public boolean isTerminated() {
    return pool.isTerminated();
  }

  /**
   * Zahájí vypnutí správce.
   */
  public void shutdown() {
    pool.shutdown();
    scheduledPool.shutdown();
  }

  /**
   * Vynutí ukončení správce.
   *
   * @return Seznam tásků, které nebyly zpracovány.
   */
  public List<Runnable> shutdownNow() {
    scheduledPool.shutdownNow();
    return pool.shutdownNow();
  }

  /**
   * Odešle tásk ke zpracování s návratovou hodnotou.
   *
   * @param callable
   * @param <T>
   *
   * @return
   */
  public <T> Future<T> submit(Callable<T> callable) {
    return pool.submit(callable);
  }

  /**
   * Odešle tásk ke zpracování s návratovou hodnotou.
   *
   * @param task
   *
   * @return
   */
  public Future<?> submit(Task task) {
    return pool.submit(task);
  }
}
