package oc.mimic.ytdl.task.core;

/**
 * Listener pro jednotlivé tásky.
 *
 * @author mimic
 */
public interface ITaskListener {

  /**
   * Tásk byl spušten.
   *
   * @param task
   */
  void started(Task task);

  /**
   * Tásk byl dokončen.
   *
   * @param task
   * @param success
   */
  void finished(Task task, boolean success);
}
