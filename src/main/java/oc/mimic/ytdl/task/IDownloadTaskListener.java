package oc.mimic.ytdl.task;

import oc.mimic.ytdl.task.core.ITaskListener;

/**
 * Listener pro stahování videa.
 *
 * @author mimic
 */
public interface IDownloadTaskListener extends ITaskListener {

  /**
   * Proces se stahováním.
   *
   * @param task
   * @param progress
   */
  void downloadProgress(DownloadTask task, String progress);

  /**
   * Stahování a zpracování selhalo.
   *
   * @param task
   * @param e
   */
  void downloadFailed(DownloadTask task, Exception e);

  /**
   * Bylo vyvoláno vynucené ukončení tásku?
   *
   * @param task
   */
  void forcedTermination(DownloadTask task);
}
