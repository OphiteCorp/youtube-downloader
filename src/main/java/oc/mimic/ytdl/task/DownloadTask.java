package oc.mimic.ytdl.task;

import oc.mimic.ytdl.ResourcesProvider;
import oc.mimic.ytdl.logic.Utils;
import oc.mimic.ytdl.logic.types.FormatType;
import oc.mimic.ytdl.task.core.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

/**
 * Tásk po stažení videa.
 *
 * @author mimic
 */
public final class DownloadTask extends Task {

  private static final Logger LOG = LoggerFactory.getLogger(DownloadTask.class);

  private static final Pattern PATTERN_DOWLOAD_STATE = Pattern
          .compile("(\\[download](.+of.+(at|in).+))|(\\[ffmpeg](.+))|(\\[youtube](.+))");

  private final DownloadTaskHandler handler = new DownloadTaskHandler();
  private final DownloadTaskConfig config;
  private final DownloadProcess downloadProcess;
  private final String tempDir;
  private final String uuid;
  private boolean stopped;

  /**
   * @param config
   */
  public DownloadTask(DownloadTaskConfig config) {
    super();
    this.config = config;

    uuid = String.format("%s-%s", UUID.randomUUID().toString().replaceAll("-", ""),
            ThreadLocalRandom.current().nextLong(10000, 99999));
    var temp = ResourcesProvider.getInstance().getTempDir();
    tempDir = Paths.get(temp.toString(), ResourcesProvider.SUBDIR_WORK, uuid).toString();
    LOG.debug("\"[{}] Created temp directory: {}", getId(), tempDir);

    downloadProcess = new DownloadProcess(this);
    LOG.info("[{}] Created a task for: {}", getId(), config.getUrlOrVideoHash());
  }

  /**
   * Přidá listener pro události nad táskem.
   *
   * @param listener
   */
  public void addDownloadListener(IDownloadTaskListener listener) {
    handler.listeners.add(listener);
  }

  /**
   * Odebere listener pro události nad táskem.
   *
   * @param listener
   */
  public void removeDownloadListener(IDownloadTaskListener listener) {
    handler.listeners.remove(listener);
  }

  /**
   * Ukončí zpracování tásku.
   */
  public void stop() {
    stopped = true;
    downloadProcess.stop();
  }

  /**
   * Zjistí, zda může být tásk ukončen bez varování (je ve stavu, kdy se ještě nezačal zpracovávat nebo je již
   * dokončený).
   *
   * @return
   */
  public boolean canBeStoppedWithoutWarning() {
    return (stopped || !downloadProcess.isRunning());
  }

  /**
   * Získá konfiguraci tásku.
   *
   * @return
   */
  public DownloadTaskConfig getConfig() {
    return config;
  }

  @Override
  protected boolean process() {
    var success = true;
    var ytDl = ResourcesProvider.getInstance().getResource(ResourcesProvider.RES_YOUTUBE_DL);

    var commands = new CommandBuilder().build(ytDl);
    var fullCommand = String.join(" ", commands);
    LOG.debug("[{}] Assembled command: {}", getId(), fullCommand);

    try {
      success = downloadProcess.download(commands);

      if (!success) {
        LOG.debug("[{}] An error occurred or the process was manually shut down", getId());
      }
    } catch (IOException e) {
      LOG.error("[" + getId() + "] An unexpected error occurred while getting a YouTube file", e);
    } catch (InterruptedException e) {
      LOG.error("[" + getId() + "] During the processing of the task, the application was terminated " + "unexpectedly",
              e);
    } finally {
      Utils.deleteDirectory(tempDir);
    }
    return success;
  }

  /**
   * Builder pro sestavení příkazu pro youtube-dl.
   */
  private final class CommandBuilder {

    /**
     * Sestaví příkaz (parametry).
     *
     * @param youtubeDlBin
     *
     * @return
     */
    private List<String> build(String youtubeDlBin) {
      var commands = new ArrayList<String>();
      commands.add(youtubeDlBin);

      prepareGlobalCommands(commands);

      if (config.getFormatType() == FormatType.AUDIO) {
        prepareOnlyAudioCommand(commands);
      } else {
        prepareVideoFormatCommand(commands);
      }
      prepareOutputDirectoryCommand(commands);

      commands.add("\"" + config.getUrlOrVideoHash() + "\"");
      return commands;
    }

    /**
     * Připraví parametry, které jsou použitelné pro všechny případy.
     *
     * @param commands
     */
    private void prepareGlobalCommands(List<String> commands) {
      if (config.getEncoding() != null) {
        commands.add("--encoding");
        commands.add(config.getEncoding());
      }
      if (config.isDownloadThumbnail()) {
        commands.add("--write-thumbnail");
      }
      if (config.isAddMetadata()) {
        commands.add("--add-metadata");
      }
    }

    /**
     * Připraví parametry pro stažení pouze audia.
     *
     * @param commands
     */
    private void prepareOnlyAudioCommand(List<String> commands) {
      var format = config.getFormat();
      var fParam = String.valueOf(format.getFormatId());

      // pokud bude vyplněn vlastní formát, tak je potřeba vždy vycházet z nejlepšího dostupnýho formátu a
      // provádět extrakci přes ffmpeg
      if (config.getCustomAudioType() != null) {
        commands.add("--extract-audio");
        commands.add("--audio-format");
        commands.add(config.getCustomAudioType().getValue());
        commands.add("--audio-quality");
        commands.add("0");

        switch (config.getCustomAudioType()) {
          case MP3:
            // mp3 vyžaduje m4a - pokud nebude k dopozici m4a, tak je možné, že se konverze nepovede, ale
            // m4a by měla být snad vždy
            fParam = "bestaudio[ext=m4a]/bestaudio";
            break;

          default:
            fParam = "bestaudio";
            break;
        }
      }
      if (config.isUseHttp()) {
        fParam = String.format("\"(%s)[protocol^=http]\"", fParam);
      } else {
        fParam = String.format("\"%s\"", fParam);
      }
      commands.add("-f");
      commands.add(fParam);
    }

    /**
     * Připraví parametry pro stažení videa.
     *
     * @param commands
     */
    private void prepareVideoFormatCommand(List<String> commands) {
      var format = config.getFormat();
      var fParam = String.valueOf(format.getFormatId());
      var extension = format.getBetterExtension();

      // existuje vlastní typ video formátu
      if (config.getCustomVideoType() != null) {
        extension = config.getCustomVideoType().getExtension();

        switch (config.getCustomVideoType()) {
          case AVI:
            // AVI bere audio stopu pouze z m4a
            fParam += "+bestaudio[ext=m4a]/bestaudio";
            break;

          default:
            throw new IllegalStateException(
                    "There is no implementation for that type of custom format: " + config.getCustomVideoType());
        }
      } else {
        // pro webm lze použít webm kvalitu zvuku
        if (format.getExtension().equalsIgnoreCase("webm")) {
          fParam += "+bestaudio[ext=webm]/bestaudio[ext=m4a]/bestaudio";
        } else {
          fParam += "+bestaudio[ext=m4a]/bestaudio";
        }
      }
      if (config.isUseHttp()) {
        fParam = String.format("\"(%s)[protocol^=http]\"", fParam);
      } else {
        fParam = String.format("\"%s\"", fParam);
      }
      commands.add("--merge-output-format");
      commands.add(extension);
      commands.add("-f");
      commands.add(fParam);
    }

    /**
     * Připraví parametry pro nastavení výstupního adresáře a názvu souboru.
     *
     * @param commands
     */
    private void prepareOutputDirectoryCommand(List<String> commands) {
      var fileName = new File(config.getOutputFile()).getName();

      // v případě audia nebo videa existují konverze a v tomto případě se nesmí shodovat typ souboru s
      // budoucím souborem po konverzi
      if (config.getCustomAudioType() != null || config.getCustomVideoType() != null) {
        var extension = Utils.getFileExtension(fileName);
        fileName = fileName.substring(0, fileName.length() - extension.length());
        fileName += "%(ext)s";
      }
      final var outputTempFile = Paths.get(tempDir, fileName).toString();
      LOG.debug("[{}] Get output file: {}", getId(), outputTempFile);

      commands.add("-o");
      commands.add("\"" + outputTempFile + "\"");
    }
  }

  /**
   * Proces pro stažení videa.
   */
  private final class DownloadProcess {

    private final DownloadTask downloadTask;
    private Process proc;
    private boolean running;

    /**
     * @param downloadTask
     */
    private DownloadProcess(DownloadTask downloadTask) {
      this.downloadTask = downloadTask;
    }

    /**
     * Vynutí ukončení procesu.
     */
    private synchronized void stop() {
      LOG.debug("[{}] Enforced quitting is required for task", getId());
      running = false;
      stopped = true;

      if (proc != null) {
        LOG.debug("[{}] Destroying process", getId());
        proc.destroyForcibly();

        // je nutné ukončit i všechny pod procesy jako třeba ffmpeg, který když je ve zpracování, tak je to
        // separátní proces a nijak se nevztahuje k aktuální aplikaci
        final var parent = ResourcesProvider.getInstance().getTempDir().toString();
        var processes = Utils.getProcessesFrom(parent);

        for (var p : processes) {
          try {
            if (p.getProcess().getCommandLine() != null && p.getProcess().getCommandLine().contains(uuid)) {
              LOG.debug("[{}] Killing children process with PID: {}", getId(), p.getPid());
              p.getHandle().destroyForcibly();
            }
          } catch (Exception e) {
            LOG.error("[{}] There was an error closing the children process", getId());
          }
        }
      }
      handler.fireForcedTermination(downloadTask);
    }

    /**
     * Vykoná příkaz dle vstupních parametrů (stáhne video nebo zvuk).
     *
     * @param commands
     *
     * @return
     *
     * @throws IOException
     * @throws InterruptedException
     */
    private boolean download(List<String> commands) throws IOException, InterruptedException {
      if (stopped) {
        return false;
      }
      var builder = new ProcessBuilder().command(commands);
      builder.redirectError(ProcessBuilder.Redirect.PIPE);
      builder.redirectOutput(ProcessBuilder.Redirect.PIPE);

      proc = builder.start();
      running = true;

      LOG.debug("[{}] The download process was started...", downloadTask.getId());
      var downloadState = true;

      try (var reader = new BufferedReader(new InputStreamReader(proc.getInputStream(), StandardCharsets.UTF_8))) {

        String line;

        while (running && (line = reader.readLine()) != null) {
          parseProcessOutput(line);
          LOG.trace(line);
        }
      }
      try (var reader = new BufferedReader(new InputStreamReader(proc.getErrorStream(), StandardCharsets.UTF_8))) {

        String line;

        while (running && (line = reader.readLine()) != null) {
          LOG.error(line);
        }
      }
      var state = proc.waitFor(); // je nutné počkat na dokončení, protože se spustil jiný process
      if (state == 0) {
        var files = new File(tempDir).listFiles();

        if (files != null) {
          // všechny soubory presune do výstupu, který si určil uživatel
          for (var f : files) {
            var path = Paths.get(new File(config.getOutputFile()).getParent(), f.getName());
            LOG.debug("[{}] Copy file '{}' to '{}'", getId(), f.getAbsolutePath(), path);
            var parent = path.toFile().getParentFile();

            if (!parent.exists()) {
              parent.mkdirs();
            }
            try {
              Utils.copyFile(f, path.toFile(), (read, current, size) -> {
                var percent = (100. / size) * current;
                handler.fireDownloadProgress(downloadTask,
                        String.format("Copying: %.2f (%s / %s)", percent, current, size));
              });
              LOG.debug("[{}] The file has been successfully copied", getId());
            } catch (IOException e) {
              LOG.error("[{}] There was an error while copying the file: {}", getId(), f.getAbsolutePath());
            }
          }
          LOG.info("[{}] The YouTube file has been downloaded", downloadTask.getId());
        } else {
          // toto by se nikdy stát nemělo, pokud stav je 0
          Utils.deleteDirectory(tempDir);
          downloadState = false;
          handler.fireDownloadFailed(downloadTask,
                  new RuntimeException("The temp directory '" + tempDir + "' is empty"));
        }
      } else {
        Utils.deleteDirectory(tempDir);
        // nastane i v případě, že manuálně proces ukončíme
        LOG.debug("[{}] The process of downloading a YouTube file has returned a different status code: " + "{}",
                downloadTask.getId(), state);
        downloadState = false;

        if (!stopped) {
          handler.fireDownloadFailed(downloadTask,
                  new RuntimeException("Invalid return code from the process: " + state));
        }
      }
      running = false;
      return downloadState;
    }

    /**
     * Je proces spuštěný?
     *
     * @return
     */
    private boolean isRunning() {
      return running;
    }

    /**
     * Parser výstupu z youtube-dl.
     *
     * @param line
     */
    private void parseProcessOutput(String line) {
      var matcher = PATTERN_DOWLOAD_STATE.matcher(line);

      if (matcher.find()) {
        var youtube = matcher.group(7);
        var download = matcher.group(2);
        var ffmpeg = matcher.group(5);

        if (ffmpeg != null) {
          ffmpeg = "Processing... Please be patient, it may take even minutes";
        }
        var progress = (youtube != null) ? youtube : ((download != null) ? download : ffmpeg);

        if (progress != null) {
          handler.fireDownloadProgress(downloadTask, progress.trim());
        }
      }
    }
  }

  /**
   * Handler pro řízení událostí.
   */
  private static final class DownloadTaskHandler {

    private final List<IDownloadTaskListener> listeners = new CopyOnWriteArrayList<>();

    /**
     * Událost selhání zpracování videa, která dá vědět všem listenerům.
     *
     * @param task
     * @param e
     */
    private synchronized void fireDownloadFailed(DownloadTask task, Exception e) {
      LOG.error("Fire downloaded failed task ID: " + task.getId(), e);
      for (var listener : listeners) {
        listener.downloadFailed(task, e);
      }
    }

    /**
     * Událost ukončení zpracování videa, která dá vědět všem listenerům.
     *
     * @param task
     */
    private synchronized void fireForcedTermination(DownloadTask task) {
      LOG.debug("Fire force termination task ID: {}", task.getId());
      for (var listener : listeners) {
        listener.forcedTermination(task);
      }
    }

    /**
     * Událost stahování videa, která dá vědět všem listenerům.
     *
     * @param task
     * @param progress
     */
    private synchronized void fireDownloadProgress(DownloadTask task, String progress) {
      LOG.debug("Fire download progress task ID: {}", task.getId());
      for (var listener : listeners) {
        listener.downloadProgress(task, progress);
      }
    }
  }
}
