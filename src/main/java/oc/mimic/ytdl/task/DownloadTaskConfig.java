package oc.mimic.ytdl.task;

import oc.mimic.ytdl.logic.json.VideoFormatJson;
import oc.mimic.ytdl.logic.types.CustomAudioType;
import oc.mimic.ytdl.logic.types.CustomVideoType;
import oc.mimic.ytdl.logic.types.FormatType;

/**
 * Konfigurace tásku pro stažení videa.
 *
 * @author mimic
 */
public final class DownloadTaskConfig {

  private String urlOrVideoHash;
  private String outputFile;
  private VideoFormatJson format;
  private FormatType formatType;
  private CustomAudioType customAudioType;
  private CustomVideoType customVideoType;
  private String encoding;
  private boolean useHttp;
  private boolean downloadThumbnail;
  private boolean addMetadata;

  public boolean isAddMetadata() {
    return addMetadata;
  }

  public void setAddMetadata(boolean addMetadata) {
    this.addMetadata = addMetadata;
  }

  public boolean isDownloadThumbnail() {
    return downloadThumbnail;
  }

  public void setDownloadThumbnail(boolean downloadThumbnail) {
    this.downloadThumbnail = downloadThumbnail;
  }

  public String getUrlOrVideoHash() {
    return urlOrVideoHash;
  }

  public void setUrlOrVideoHash(String urlOrVideoHash) {
    this.urlOrVideoHash = urlOrVideoHash;
  }

  public String getOutputFile() {
    return outputFile;
  }

  public void setOutputFile(String outputFile) {
    this.outputFile = outputFile;
  }

  public CustomAudioType getCustomAudioType() {
    return customAudioType;
  }

  public void setCustomAudioType(CustomAudioType customAudioType) {
    this.customAudioType = customAudioType;
  }

  public FormatType getFormatType() {
    return formatType;
  }

  public void setFormatType(FormatType formatType) {
    this.formatType = formatType;
  }

  public VideoFormatJson getFormat() {
    return format;
  }

  public void setFormat(VideoFormatJson format) {
    this.format = format;
  }

  public String getEncoding() {
    return encoding;
  }

  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

  public boolean isUseHttp() {
    return useHttp;
  }

  public void setUseHttp(boolean useHttp) {
    this.useHttp = useHttp;
  }

  public CustomVideoType getCustomVideoType() {
    return customVideoType;
  }

  public void setCustomVideoType(CustomVideoType customVideoType) {
    this.customVideoType = customVideoType;
  }
}
